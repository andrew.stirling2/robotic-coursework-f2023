#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <Eigen/Dense>


class BaseController{
    public:
        // Constructor
        BaseController(ros::NodeHandle& nh);

        void done_sub_cb(const std_msgs::Bool reached);
        void serve_sub_cb(const std_msgs::Bool serve);
        void pose_sub_cb(const geometry_msgs::Pose des_pose);
        void twist_sub_cb(const geometry_msgs::Twist des_twist);
        void husky_pose_sub_cb(const geometry_msgs::Pose husky_pose);

        void compute_husky_vel();

        void update();

    protected:

        bool read_parameters();

    private:
        //ROS Stuff
        ros::NodeHandle& node_handle_ ;
        ros::Subscriber done_sub_;
        ros::Subscriber serve_sub_;
        ros::Subscriber pose_sub_;
        ros::Subscriber twist_sub_;
        ros::Subscriber husky_pose_sub_;
        ros::Publisher husky_vel_pub_;

        //Parameters
        double max_lin_vel_;
        double max_ang_vel_;
        double x_target_;
        double y_target_;

        // Subscriber Storage Variables
        bool can_serve_;
        bool done_;
        geometry_msgs::Pose des_pose_;
        geometry_msgs::Twist des_twist_;
        geometry_msgs::Pose husky_cur_pose_;

        // Publish Value
        geometry_msgs::Twist husky_cmd_vel_;

        //Computation Vars
        geometry_msgs::Quaternion q_msg;
        tf2::Quaternion q_tf2;
        double roll;
        double pitch;
        double yaw_cur;
        double yaw_des;
        double omega;
        double lin_vel;

        // Matrices and Vectors
        Eigen::Vector2d x_r;
        Eigen::Matrix2d R;
        Eigen::Matrix2d J;
        Eigen::Vector2d vel;
        Eigen::Vector2d cmd_vel;
};
