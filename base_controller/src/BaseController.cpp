#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <Eigen/Dense>
#include <cmath>
#include <base_controller/BaseController.hpp>

BaseController::BaseController(ros::NodeHandle& nh) : node_handle_(nh), done_(true){
    //Init subscribers
    done_sub_ = node_handle_.subscribe("/planner/done",1, &BaseController::done_sub_cb,this);
    serve_sub_ = node_handle_.subscribe("/planner/serve",1, &BaseController::serve_sub_cb,this);
    pose_sub_ = node_handle_.subscribe("/planner/pose",1, &BaseController::pose_sub_cb,this);
    twist_sub_ = node_handle_.subscribe("/planner/twist",1, &BaseController::twist_sub_cb,this);
    husky_pose_sub_ = node_handle_.subscribe("/husky_velocity_controller/feedback/pose",1, &BaseController::husky_pose_sub_cb,this);

    husky_vel_pub_ = node_handle_.advertise<geometry_msgs::Twist>("/husky_velocity_controller/cmd_vel",1);

    if (read_parameters()) {
		ROS_INFO("Successfully Read Parameters");
	}
    ROS_INFO("Connected Base Controller Node");

}
void BaseController::done_sub_cb(const std_msgs::Bool reached){
    done_ = reached.data;
}
void BaseController::serve_sub_cb(const std_msgs::Bool serve){
    can_serve_ = serve.data;
}
void BaseController::pose_sub_cb(const geometry_msgs::Pose pose){
    des_pose_ = pose;
}
void BaseController::twist_sub_cb(const geometry_msgs::Twist twist){
    des_twist_ = twist;
}
void BaseController::husky_pose_sub_cb(const geometry_msgs::Pose husky_pose){
    husky_cur_pose_ = husky_pose;
}

void BaseController::compute_husky_vel(){
    // Get yaw
    q_msg = husky_cur_pose_.orientation;
    tf2::fromMsg(q_msg, q_tf2);
    tf2::Matrix3x3 rotation(q_tf2);
    rotation.getRPY(roll, pitch, yaw_cur);

    // // Method 1
    // // Calculate Residual 
    
    x_r(0) = x_target_ - husky_cur_pose_.position.x;
    x_r(1) = y_target_ - husky_cur_pose_.position.y;
    // ROS_INFO_STREAM("x_r = " << x_r(0) << " , " << x_r(1));
    
    R(0,0) = cos(yaw_cur);
    R(1,0) = -sin(yaw_cur);
    R(0,1) = sin(yaw_cur);
    R(1,1) = cos(yaw_cur);
    x_r = R * x_r;

    // Inverse Jacobian Calculation
    J(0,0) = (x_r(0)*cos(yaw_cur)) - (x_r(1)*sin(yaw_cur));
    J(1,0) = -sin(yaw_cur);
    J(0,1) = (sin(yaw_cur)*x_r(0)) + ((cos(yaw_cur)*x_r(1)));
    J(1,1) =  cos(yaw_cur)*x_r(0);
    // J(0,0) = cos(yaw_cur);
    // J(1,0) = sin(yaw_cur);
    // J(0,1) = (-sin(yaw_cur)*x_r(0)) - (cos(yaw_cur)*x_r(1));
    // J(1,1) =  (cos(yaw_cur)*x_r(0)) - (sin(yaw_cur)*x_r(1));

    // Calculate linear velocity and angular velocity
    vel(0) = des_twist_.linear.x;
    vel(1) = des_twist_.linear.y;
    
    cmd_vel = (1/x_r(0)) * J * vel;
    // cmd_vel = J.inverse() * vel;
    lin_vel = cmd_vel(0);
    omega = cmd_vel(1);

    // // Method 2
    // // Get desired yaw
    // yaw_des = atan2(des_twist_.linear.y, des_twist_.linear.x);
    // omega = (yaw_des - yaw_cur) * 500;
    // lin_vel = sqrt(pow(des_twist_.linear.x,2) + pow(des_twist_.linear.y,2));
    
    // Normalize
    if (abs(omega) > max_ang_vel_){
        omega = omega * max_ang_vel_ / abs(omega);
    }
    if (abs(lin_vel) > max_lin_vel_){
        lin_vel = lin_vel * max_lin_vel_/ abs(lin_vel);
    }
    
    husky_cmd_vel_.linear.x = lin_vel;
    husky_cmd_vel_.angular.z = omega;
    // ROS_INFO("At position: (%f , %f)", husky_cur_pose_.position.x , husky_cur_pose_.position.y);
    // ROS_INFO("Velocities: linear = %f, angular = %f", lin_vel, omega);
}
void BaseController::update(){
    if (can_serve_ && !done_){
        compute_husky_vel();
    }
    else{
        husky_cmd_vel_.linear.x = 0;
        husky_cmd_vel_.angular.z = 0;
    }
    husky_vel_pub_.publish(husky_cmd_vel_);
}

bool BaseController::read_parameters() {
    if ( !node_handle_.getParam("/husky_velocity_controller/linear/x/max_velocity", max_lin_vel_)) {
		ROS_INFO("Cannot find max linear velocity" );
		return false ;
	}
    if ( !node_handle_.getParam("/husky_velocity_controller/angular/z/max_velocity", max_ang_vel_)) {
		ROS_INFO("Cannot find max angular velocity" );
		return false ;
	}
    if ( !node_handle_.getParam("target/x", x_target_)) {
		ROS_INFO("Cannot x target" );
		return false ;
	}
    if ( !node_handle_.getParam("target/y", y_target_)) {
		ROS_INFO("Cannot y target" );
		return false ;
	}
    ROS_INFO("Target Postion: x = %f, y = %f", x_target_, y_target_);
    ROS_INFO("Max Velocities: lin= %f, ang= %f", max_lin_vel_, max_ang_vel_);
    return true;
}

/*
Questions for Prof:
- Difference between using J.inverse() and computing inverse given in textbook p. 527
- Difference between method 1 and 2
- When to publish to /planner/done
- Body frame of reference
*/