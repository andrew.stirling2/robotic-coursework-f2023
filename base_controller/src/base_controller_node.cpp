#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>
#include <base_controller/BaseController.hpp>


int main(int argc, char **argv){
    // initialize ROS
	ros::init(argc, argv, "controller");

    ros::NodeHandle nh;
    
    ros::Rate loopRate(500);
    
    BaseController controller(nh);


    while (ros::ok()){
        // Call callbacks
        ros::spinOnce();
        // Plan and publish
        controller.update();
        
        loopRate.sleep();
    }


    return 0;
}