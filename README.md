# robotic-coursework-f2023

## Assignment 8

### Pick and Place Procedure

#### Gripper Positions:

For the boxes, I define closed as 0.62 and open as 0.1, respectively. To grasp the box I set the effort to 2.0. 

With the "bartender bot", the closed position is 0.183 with an effor of 4.0. Because the cup is round and the gripper is not compliant, the gripper end up pinching the cup. This resulted in the cup swinging slighty when moved, so I had to be very delicated while carrying it.

#### End-Effector Targets:

For the boxes, the end-effector is set to a translation of [0.35, 0.0, 0.44]. And orientation of [0.0, 0.0, 1.5], representing the roll, pitch and yaw angles, respectively. For other boxes, I set the x, and y position to the location of the box in gazebo, and kept the z value equal to the sum of the table height and gripper length (0.44 m). 

When grasping the cup, I had to change the end-effector target to a translation of [0.445, -0.083, 0.31] and orientation of [1.57, 0.0, 0.0]. In this pose, the arm grabs the cup from the side, slowly lifting it up. 

#### Results:

##### Bartender Bot
Here is the [Bartender Bot](https://youtu.be/0jd9S7HlYWA).

You can see the arm collecting and placing "ice cubes" in a glass, and then moving the glass to the edge of the table, ready to serve someone! 

##### Regular Pick and Place Task
Here is the simple [pick and place](https://youtu.be/5OPJjx0QG30) task, as specified in the assignment description.

Each version works as expected each time, despite the block occasionally slipping slightly in the gripper. 