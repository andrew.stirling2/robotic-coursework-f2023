// #pragma GCC diagnostic push
// #pragma GCC diagnostic ignored "-Wignored-attributes"
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
#include <pinocchio/math/rpy.hpp>
#include <pinocchio/math/quaternion.hpp>
// #pragma GCC diagnostic pop
#include <Eigen/Dense>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <cmath>
#include <highlevel_msgs/MoveTo.h>
#include <highlevel_controller/CubicPlanner.hpp>

CubicPlanner::CubicPlanner(ros::NodeHandle& nh) : node_handle_(nh), JOINT_ID(7), T_(4.5), has_start_pos_(false), arrived_(false), move_ori_called(false){
    // Subscriber Init
    sub_ = node_handle_.subscribe("/gen3/joint_states", 1, &CubicPlanner::sub_callback,this);

    // Service Init
    service_ = node_handle_.advertiseService("pose_planner/move_to", &CubicPlanner::service_callback,this);
    service_ori = node_handle_.advertiseService("pose_planner/move_ori", &CubicPlanner::service_callback_ori,this);
    // Publisher Initialization
    ang_pub_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/gen3/joint_group_position_controller/command",1);

    target_.resize(3,0.0);
    
    des_ang_pos_.resize(7,0.0);
    cur_joint_pos_.resize(7,100.0);
    cur_joint_vel_.resize(7,0.0);

    vel_limits_[0] = 1.3963;
    vel_limits_[1] = 1.3963;
    vel_limits_[2] = 1.3963;
    vel_limits_[3] = 1.3963;
    vel_limits_[4] = 1.2218;
    vel_limits_[5] = 1.2218;
    vel_limits_[6] = 1.2218;

    eig_start_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_target_joint = Eigen::VectorXd::Zero(7);
    eig_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_joint_vel_ = Eigen::VectorXd::Zero(7);
    eig_des_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_des_joint_vel_ = Eigen::VectorXd::Zero(7);
    eig_des_pos = Eigen::Vector3d::Zero();
    eig_des_vel = Eigen::Vector3d::Zero();
    eig_target = Eigen::Vector3d::Zero();
    eig_start_pos = Eigen::Vector3d::Zero();
    eig_null_joint = Eigen::VectorXd::Zero(7);
    eig_arrived_joint_pos = Eigen::VectorXd::Zero(7);
    eig_cur_pos = Eigen::Vector3d::Zero();
    eig_full_vel = Eigen::VectorXd::Zero(6);
    
    jacobian_local_world = Eigen::MatrixXd::Zero(6, JOINT_ID);
    jacobian_local_translation = Eigen::MatrixXd::Zero(3,JOINT_ID);
    jacobian_local_pseudo = Eigen::MatrixXd::Zero(JOINT_ID,3);
    jacobian_full_pseudo = Eigen::MatrixXd::Zero(JOINT_ID,6);
    t_initial_ = ros::Time::now().toSec();

    ROS_INFO("Reading Parameters...");
    if(readParamaters()){
        ROS_INFO("Succesfully Read Parameters!");
        pinocchio::urdf::buildModel(urdf_file_, model, false);
        data = pinocchio::Data(model);
    }
    
    for(int i =0;i<7;i++){
        eig_target_joint(i) = target_joints[i];
    }
    euler_target(0) = target_orientations[0];
    euler_target(1) = target_orientations[1];
    euler_target(2) = target_orientations[2];
    ROS_INFO_STREAM("Target Orientation: "<<euler_target.transpose());
    ROS_INFO_STREAM("k_ori_vel: "<<k_ori_vel << " k_joint_vel: "<<k_joint_vel);
    ROS_INFO_STREAM("k_null: "<<k_null << " k_att: "<<k_att);
    ROS_INFO("Succesfully connected with Cubic Planner");
}

bool CubicPlanner::service_callback(highlevel_msgs::MoveTo::Request  &req, highlevel_msgs::MoveTo::Response &res){
    if (req.z < 0) {
        res.can_execute = false;
        ROS_INFO("move_to z position must be greater than 0");
        return false;
    }
    if (req.T <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to period T must be greater than 0");
        return false;
    }
    target_[0] = req.x;
    target_[1] = req.y;
    target_[2] = req.z;
    T_ = req.T;
    res.can_execute = true;
    has_start_pos_ = false;
    arrived_ = false;
    move_ori_called = false;
    debug = 1;
    // ROS_INFO_STREAM("Current Position: " << eig_cur_pos.transpose());
    
    return true;
}
bool CubicPlanner::service_callback_ori(highlevel_msgs::MoveTo::Request  &req, highlevel_msgs::MoveTo::Response &res){
    if (req.T <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to period T must be greater than 0");
        return false;
    }
    target_orientations[0] = req.x;
    target_orientations[1] = req.y;
    target_orientations[2] = req.z;
    T_ = req.T;
    res.can_execute = true;
    has_start_pos_ = false;
    arrived_ = false;
    move_ori_called = true;
    // debug = 1;
    
    return true;
}
void CubicPlanner::sub_callback(const sensor_msgs::JointState cur_state){

    cur_joint_pos_ = cur_state.position;
    cur_joint_vel_ = cur_state.velocity;

    for (int i = 0; i<cur_joint_pos_.size();i++){
        eig_joint_ang_(i) = cur_joint_pos_[i];
        eig_joint_vel_(i) = cur_joint_vel_[i];
    }
    
    
}
void CubicPlanner::compute_ori(){
    rotmat_fbk = data.oMi[JOINT_ID].rotation();
    pinocchio::quaternion::assignQuaternion(quat_fbk,rotmat_fbk);
    euler_fbk = pinocchio::rpy::matrixToRpy(rotmat_fbk);

    if(move_ori_called){
        quat_ref = quat_start.slerp(time_coeff_,quat_target);
        rotmat_ref = quat_ref.toRotationMatrix();
        euler_ref = pinocchio::rpy::matrixToRpy(rotmat_ref);
        euler_ref_vel = k_ori_vel * pinocchio::rpy::matrixToRpy((quat_ref*quat_fbk.inverse()).normalized().toRotationMatrix());
    }
    else{
        euler_ref_vel = k_ori_vel * pinocchio::rpy::matrixToRpy((quat_target*quat_fbk.inverse()).normalized().toRotationMatrix());
    }
    
    if(cur_time_>T_){
        euler_ref_vel = k_ori_vel * pinocchio::rpy::matrixToRpy((quat_target*quat_fbk.inverse()).normalized().toRotationMatrix());
    }

    // if (euler_ref_vel.norm()>max_ang_vel){
    //     euler_ref_vel = euler_ref_vel/euler_ref_vel.norm() * max_ang_vel;
    // }

}
void CubicPlanner::compute_traj(){
    if (! has_start_pos_){
        // Assign start position of hand
        compute_start_pos();
        has_start_pos_ = true;
        // ROS_INFO_STREAM("Desired Position: " << eig_target.transpose());
    }

    offset_time_ = ros::Time::now().toSec();
    cur_time_ =  offset_time_ - t_initial_;
    time_coeff_ = (3*pow(cur_time_,2)/pow(T_,2)) - (2*pow(cur_time_,3)/pow(T_,3));

    eig_des_pos = eig_start_pos + (time_coeff_*(eig_target - eig_start_pos));

    pinocchio::forwardKinematics(model,data,eig_joint_ang_,eig_joint_vel_);
    eig_cur_pos = data.oMi[JOINT_ID].translation();
    eig_des_vel = (eig_des_pos - eig_cur_pos) * pub_rate_;
    
    if (cur_time_ > T_){
        eig_des_pos = eig_target;
        eig_des_vel = k_att*(eig_target - eig_cur_pos);
        // ROS_INFO_STREAM("CURRENT HAND POSITION: \n "<< eig_cur_pos);
    }

    if (eig_des_vel.norm()>max_lin_vel){
        eig_des_vel = eig_des_vel/eig_des_vel.norm() * max_lin_vel;
    }
    compute_ori();

    eig_full_vel(0) = eig_des_vel(0);
    eig_full_vel(1) = eig_des_vel(1);
    eig_full_vel(2) = eig_des_vel(2);

    eig_full_vel(3) = euler_ref_vel(0);
    eig_full_vel(4) = euler_ref_vel(1);
    eig_full_vel(5) = euler_ref_vel(2);

    double position_err = (eig_target-eig_cur_pos).norm();
    double ori_err = quat_fbk.angularDistance(quat_target);

    if(position_err<0.01){
        if (ori_err<0.08){
            if(!arrived_){
                ROS_INFO_STREAM("Arrived at [ "<< eig_cur_pos.transpose() << " ] in "<< cur_time_ << "s");
                // eig_arrived_joint_pos = eig_cur_pos;
                debug = 0;
                move_ori_called = false;
                arrived_ = true;
                ROS_INFO_STREAM("Position Error: "<< position_err);
                ROS_INFO_STREAM("Orientation Error: "<< ori_err);
            }
            // eig_arrived_joint_pos = eig_joint_ang_;
        }
        else{
            arrived_ = false;
        }
    }
    else{
        arrived_ = false;
    }
    if(debug){
        // ROS_INFO_STREAM("Target Orientation: " << euler_target.transpose());
        ROS_INFO_STREAM("Current Orientation: " << euler_fbk.transpose());
        // ROS_INFO_STREAM("Ref Orientation: "<<euler_ref.transpose());
        ROS_INFO_STREAM("Current Hand Position: " << eig_cur_pos.transpose());
        // ROS_INFO_STREAM("Orientation Error: "<< ori_err);
        ROS_INFO_STREAM("Full Velocity: "<< eig_full_vel.transpose());
        ROS_INFO_STREAM("Target Hand Position: "<< eig_target.transpose());
        // ROS_INFO_STREAM("CURRENT ANGULAR POSITION: \n "<< eig_joint_ang_);
        // ROS_INFO_STREAM("DESIRED ANGULAR POSITION: \n"<< eig_des_joint_ang_);
        // ROS_INFO_STREAM("DESIRED HAND POSITION: \n"<< eig_des_pos);
        // ROS_INFO_STREAM("ADJUSTED TIME: "<< cur_time_  << " INITIAL TIME: " << t_initial_);
        // ROS_INFO_STREAM("ARRIVED: " << std::boolalpha << arrived_);
    }  
    // ROS_INFO_STREAM("Position Error: "<< position_err);
    // ROS_INFO_STREAM("Orientation Error: "<< ori_err);
    return;
}

void CubicPlanner::compute_inv_kin(){

    pinocchio::computeAllTerms(model, data, eig_joint_ang_, eig_joint_vel_);
    pinocchio::getJointJacobian(model, data, JOINT_ID, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_local_world);
    
    jacobian_local_translation = jacobian_local_world.topRows(3);
    jacobian_local_pseudo = jacobian_local_translation.transpose() * (jacobian_local_translation * jacobian_local_translation.transpose()).inverse();
    
    jacobian_full_pseudo = jacobian_local_world.transpose() * (jacobian_local_world * jacobian_local_world.transpose()).inverse();
    // eig_des_joint_vel_ = jacobian_local_pseudo * eig_des_vel;
    eig_des_joint_vel_ = jacobian_full_pseudo * eig_full_vel;

    // Eigen::MatrixXd jacobian_nullspace = Eigen::MatrixXd::Identity(7,7) - (jacobian_local_pseudo*jacobian_local_translation);
    Eigen::MatrixXd jacobian_nullspace = Eigen::MatrixXd::Identity(7,7) - (jacobian_full_pseudo*jacobian_local_world);

    eig_null_joint = k_null*(eig_target_joint - eig_joint_ang_);

    eig_des_joint_vel_ = eig_des_joint_vel_ + (jacobian_nullspace*eig_null_joint);

    double eig_element;
    for(int i = 0; i<7;i++){
        eig_element = eig_des_joint_vel_(i);

        if(abs(eig_element) > vel_limits_[i]){
            eig_des_joint_vel_(i) = eig_element/abs(eig_element)*vel_limits_[i];
            // ROS_INFO("Exceeded velocity limits");
        }

        eig_des_joint_ang_(i) = eig_joint_ang_(i) + k_joint_vel*(eig_des_joint_vel_(i) / pub_rate_);
        des_ang_pos_[i] = eig_des_joint_ang_(i);
    }
    // if (arrived_){
    //     for(int i = 0; i<7;i++){
    //         des_ang_pos_[i] = eig_arrived_joint_pos(i);
    //     }
    //     // ROS_INFO_STREAM("Arrived Position: "<< eig_arrived_joint_pos.transpose());
    // }
    return;

}

void CubicPlanner::compute_start_pos(){
    // Translations
    eig_start_joint_ang_ = eig_joint_ang_;
    pinocchio::forwardKinematics(model,data,eig_joint_ang_,eig_joint_vel_);
    pinocchio::SE3 pose_now = data.oMi[JOINT_ID];
    eig_start_pos = pose_now.translation();
    eig_target(0) = target_[0];
    eig_target(1) = target_[1];
    eig_target(2) = target_[2];

    // Orientations 
    // Assign Startng positions
    rotmat_start = pose_now.rotation();
    pinocchio::quaternion::assignQuaternion(quat_start,rotmat_start);
    euler_target(0) = target_orientations[0];
    euler_target(1) = target_orientations[1];
    euler_target(2) = target_orientations[2];

    rotmat_target = pinocchio::rpy::rpyToMatrix(euler_target(0),euler_target(1),euler_target(2));
    pinocchio::quaternion::assignQuaternion(quat_target,rotmat_target);
    pinocchio::quaternion::assignQuaternion(quat_start,rotmat_start);

    t_initial_ = ros::Time::now().toSec();

    // ROS_INFO_STREAM("Quat Target: "<< quat_target.vec());
    // ROS_INFO_STREAM("Quat Start: " << quat_start.vec());
    
}

void CubicPlanner::update(){
    
    compute_traj(); // Compute desired twist in task space
    compute_inv_kin(); // Compute desired joint_position 

    cmd_ang_pose_.data = des_ang_pos_;
    ang_pub_.publish(cmd_ang_pose_);
    
    
}

bool CubicPlanner::readParamaters(){
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/hand/position", target_)) {
		ROS_INFO("Can't read target hand positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/hand/orientation", target_orientations)) {
		ROS_INFO("Can't read target hand orientations");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/joint/positions", target_joints)) {
		ROS_INFO("Can't read target joint positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_)) {
		ROS_INFO("Can't read urdf file name");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/linear/max_velocity", max_lin_vel)) {
		ROS_INFO("Can't read max velocity");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/angular/max_velocity", max_ang_vel)) {
		ROS_INFO("Can't read max velocity");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_null", k_null)) {
		ROS_INFO("Can't read k_null");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_att", k_att)) {
		ROS_INFO("Can't read k_att");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_joint_vel", k_joint_vel)) {
		ROS_INFO("Can't read k_joint_vel");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_ori_vel", k_ori_vel)) {
		ROS_INFO("Can't read k_ori_vel");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/debug", debug)) {
		ROS_INFO("Can't read debug");
		return false;
	}
    ROS_INFO("Target Hand Position: [%f,%f,%f]", target_[0], target_[1], target_[2]);
    ROS_INFO("Target Hand Orientation: [%f,%f,%f]", target_orientations[0], target_orientations[1], target_orientations[2]);
    ROS_INFO_STREAM("Publishing at: " << pub_rate_ << " Hz");
    ROS_INFO_STREAM("URDF File found at: " << urdf_file_);
    return true;

}

bool CubicPlanner::ready(){
    if(ros::Time::now().toSec() == 0){
        return false;
    }
    if(cur_joint_pos_[0] > 90.0){
        return false;
    }
    return true;
}