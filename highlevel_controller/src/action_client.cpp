#include <ros/ros.h>
#include <highlevel_controller/ActionClient.hpp>


int main(int argc, char** argv){
    ros::init(argc,argv, "action_client");
    ros::NodeHandle nh;

    ActionClient action_client(nh);
    // int pub_rate;
    // nh.getParam("/publish_rate",pub_rate);
    ros::Rate loop_rate(500);
    
    while ( ros::ok() ) {
        // if (!action_client.ready()){
        //     ros::spinOnce();
        //     loop_rate.sleep() ;
        // }
        // else{
        //     ros::spinOnce();
        //     action_client.update() ;
        //     loop_rate.sleep();
        // }
        ros::spinOnce();
        action_client.update() ;
        loop_rate.sleep();
		
	}
    return 0;
}