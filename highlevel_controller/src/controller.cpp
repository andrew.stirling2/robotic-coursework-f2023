#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <highlevel_msgs/MoveTo.h>
#include <highlevel_controller/Controller.hpp>

int main(int argc, char **argv){
    // initialize ROS
	ros::init(argc, argv, "task_controller");

    ros::NodeHandle nh;

    double pub_rate;
    nh.getParam("/publish_rate",pub_rate);
    ros::Rate loopRate(pub_rate);
    
    Controller controller(nh);
    int count = 0;
    while (ros::ok()){
        if (! controller.ready()){
            ros::spinOnce();
            loopRate.sleep();
        }
        else{
            // Call callbacks
            ros::spinOnce();
            // Plan and publish
            controller.update();
            loopRate.sleep();
        }
        // // Call callbacks
        // ros::spinOnce();
        // // Plan and publish
        // controller.update();
        // loopRate.sleep();
    }
    
    return 0;
}