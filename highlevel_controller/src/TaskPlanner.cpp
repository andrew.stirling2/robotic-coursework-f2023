// #pragma GCC diagnostic push
// #pragma GCC diagnostic ignored "-Wignored-attributes"
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
#include <pinocchio/math/rpy.hpp>
// #pragma GCC diagnostic pop
#include <Eigen/Dense>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <cmath>
#include <highlevel_msgs/MoveTo.h>
#include <highlevel_controller/TaskPlanner.hpp>


TaskPlanner::TaskPlanner(ros::NodeHandle& nh) : node_handle_(nh), JOINT_ID(7), T_(4.5), has_start_pos_(false), arrived_(false), service_called(false){
    // Subscriber Init
    sub_ = node_handle_.subscribe("/gen3/joint_states", 1, &TaskPlanner::sub_callback,this);

    // Service Init
    service_ = node_handle_.advertiseService("pose_planner/move_to", &TaskPlanner::service_callback,this);

    service_orientation_ = node_handle_.advertiseService("pose_planner/move_ori", &TaskPlanner::service_callback_orientation,this);
    // Publisher Initialization
    torque_pub_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/gen3/joint_group_effort_controller/command",1);
    
    // Parameter Initialize
    target_.resize(3,0.0);
    target_orientations.resize(3,0.0);
    des_torque_.resize(7,0.0);
    cur_joint_pos_.resize(7,100.0);
    cur_joint_vel_.resize(7,0.0);
    // ROS_INFO("Starting Joints");
    // Joint Stuff
    eig_start_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_target_joint = Eigen::VectorXd::Zero(7);
    eig_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_joint_vel_ = Eigen::VectorXd::Zero(7);
    // ROS_INFO("Starting Translations");
    // Translations
    eig_cur_pos = Eigen::Vector3d::Zero();
    eig_target = Eigen::Vector3d::Zero();
    eig_des_pos = Eigen::Vector3d::Zero();
    eig_des_vel = Eigen::Vector3d::Zero();
    eig_fbk_vel = Eigen::Vector3d::Zero();
    eig_des_acc = Eigen::Vector3d::Zero();

    // Nullstuff
    eig_null_torque = Eigen::VectorXd::Zero(7);
    eig_arrived_torque = Eigen::VectorXd::Zero(7);
    // ROS_INFO("Starting Jacobians");
    // Jacobians
    jacobian_local_world = Eigen::MatrixXd::Zero(6, JOINT_ID);
    jacobian_local_translation = Eigen::MatrixXd::Zero(3,JOINT_ID);
    jacobian_local_pseudo = Eigen::MatrixXd::Zero(JOINT_ID,6);
    jacobian_dot = Eigen::MatrixXd::Zero(6,JOINT_ID);
    jacobian_dot_translation = Eigen::MatrixXd::Zero(3,JOINT_ID);
    // ROS_INFO("Starting Orientations");
    // Orientations
    euler_pos_err = Eigen::Vector3d::Zero();

    // Stacked Stuff
    eig_des_acc_stacked = Eigen::VectorXd::Zero(6);
    end_effec_wrench = Eigen::VectorXd::Zero(6);


    t_initial_ = ros::Time::now().toSec();

    ROS_INFO("Reading Parameters...");
    if(readParamaters()){
        ROS_INFO("Succesfully Read Parameters!");
        pinocchio::urdf::buildModel(urdf_file_, model, false);
        data = pinocchio::Data(model);
    }
    
    for(int i =0;i<7;i++){
        eig_target_joint(i) = target_joints[i];
    }
    eig_target(0) = target_[0];
    eig_target(1) = target_[1];
    eig_target(2) = target_[2];
    euler_target(0) = target_orientations[0];
    euler_target(1) = target_orientations[1];
    euler_target(2) = target_orientations[2];
    ori_ref_mat = pinocchio::rpy::rpyToMatrix(euler_target(0),euler_target(1),euler_target(2));
    
    ROS_INFO("Succesfully connected with Cubic Planner");
    ROS_INFO_STREAM("Using k_att = "<< k_att_);
    ROS_INFO_STREAM("Using k_p_field = "<< Kp);
    ROS_INFO_STREAM("Using k_d_field = "<< Kd);
    ROS_INFO_STREAM("Using k_p_null = "<< Kp_null);
    ROS_INFO_STREAM("Using k_acc = "<< k_acc_);
}

bool TaskPlanner::service_callback(highlevel_msgs::MoveTo::Request  &req, highlevel_msgs::MoveTo::Response &res){
    if (req.z < 0) {
        res.can_execute = false;
        ROS_INFO("move_to z position must be greater than 0");
        return false;
    }
    if (req.T <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to period T must be greater than 0");
        return false;
    }
    target_[0] = req.x;
    target_[1] = req.y;
    target_[2] = req.z;
    eig_target(0) = target_[0];
    eig_target(1) = target_[1];
    eig_target(2) = target_[2];
    T_ = req.T;
    res.can_execute = true;
    has_start_pos_ = false;
    arrived_ = false;
    service_called = true;
    // ROS_INFO_STREAM("Current Position: " << eig_cur_pos.transpose());
    

    return true;
}

bool TaskPlanner::service_callback_orientation(highlevel_msgs::MoveTo::Request  &req, highlevel_msgs::MoveTo::Response &res){
    if (req.T <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to period T must be greater than 0");
        return false;
    }
    target_orientations[0] = req.x;
    target_orientations[1] = req.y;
    target_orientations[2] = req.z;
    euler_target(0) = target_orientations[0];
    euler_target(1) = target_orientations[1];
    euler_target(2) = target_orientations[2];
    ori_ref_mat = pinocchio::rpy::rpyToMatrix(euler_target(0),euler_target(1),euler_target(2));
    T_ = req.T;
    res.can_execute = true;
    has_start_pos_ = false;
    arrived_ = false;
    service_called = true;
    return true;
}

void TaskPlanner::sub_callback(const sensor_msgs::JointState cur_state){

    cur_joint_pos_ = cur_state.position;
    cur_joint_vel_ = cur_state.velocity;
    for (int i = 0; i<cur_joint_pos_.size();i++){
        eig_joint_ang_(i) = cur_joint_pos_[i];
        eig_joint_vel_(i) = cur_joint_vel_[i];
    }

}

void TaskPlanner::stack(){
    // Transation Acceleration
    eig_des_acc = (Kd*(eig_des_vel-eig_fbk_vel)) + (Kp*(eig_des_pos - eig_cur_pos));
    
    
    // Orientations
    euler_des_acc = (Kd_ori*(euler_vel_ref-euler_fbk_vel))+ (Kp_ori*(euler_pos_err));
    // euler_des_acc = (Kd_ori*(euler_vel_ref-euler_fbk_vel))+ (Kp_ori*(euler_target - euler_fbk));

    eig_des_acc_stacked(0) = eig_des_acc(0);
    eig_des_acc_stacked(1) = eig_des_acc(1);
    eig_des_acc_stacked(2) = eig_des_acc(2);

    eig_des_acc_stacked(3) = euler_des_acc(0);
    eig_des_acc_stacked(4) = euler_des_acc(1);
    eig_des_acc_stacked(5) = euler_des_acc(2);
    // eig_des_pos_stack.block<3,1>(0,0) = eig_des_pos;
    // eig_des_pos_stack.block<3,1>(3,0) = euler_des_pos;
}
void TaskPlanner::compute_inv_dyn(){
    // ROS_INFO_STREAM("Using Kp = "<< Kp);
    // ROS_INFO_STREAM("Using Kd = "<< Kd);

    // ROS_INFO_STREAM("position error: "<<(eig_des_pos-eig_cur_pos).transpose());
    // ROS_INFO_STREAM("velocity error: "<<(eig_des_vel-eig_fbk_vel).transpose());
    // ROS_INFO_STREAM("x_ddot reference: "<<eig_des_acc.transpose());

    // eig_des_acc = eig_des_acc + (Kd*(eig_des_vel-eig_fbk_vel)) + (Kp*(eig_des_pos - eig_cur_pos));
    // Lambda Matrix

    end_effec_wrench = jacobian_local_pseudo.transpose()* data.M * jacobian_local_pseudo * eig_des_acc_stacked;
    // Eta Matrix
    end_effec_wrench = end_effec_wrench + (jacobian_local_pseudo.transpose()*data.nle) -  jacobian_local_pseudo.transpose()* data.M * jacobian_local_pseudo * jacobian_dot * eig_joint_vel_;
    eig_cmd_torque = jacobian_local_world.transpose()*end_effec_wrench;

    // ROS_INFO_STREAM("cmd torque pre null: " << eig_cmd_torque.transpose());
    
    // Add Redundancy
    eig_null_torque = (data.M * (Kp_null * (eig_target_joint - eig_joint_ang_) + Kd*(-1*eig_joint_vel_))) + data.nle;
    eig_null_torque = jacobian_nullspace*eig_null_torque;
    // ROS_INFO_STREAM("Cmd Torque: " << eig_cmd_torque.transpose());
    eig_cmd_torque = eig_cmd_torque + eig_null_torque;
    // ROS_INFO_STREAM("cmd torque post null: " << eig_cmd_torque.transpose());
    // ROS_INFO_STREAM("Current hand position: " << eig_cur_pos.transpose());
    for (int i = 0; i<7;i++){
        des_torque_[i] = eig_cmd_torque(i);
    }
    check_arrived();
    // if (arrived_){
    //     for (int i = 0; i<7;i++){
    //         des_torque_[i] = eig_arrived_torque(i);
    //     }
    // }
    // ROS_INFO_STREAM("Null Torque: " << eig_null_torque.transpose());
    
    // ROS_INFO_STREAM("Current Orientation: "<< euler_fbk.transpose());
    // ROS_INFO_STREAM("ARRIVED: " << std::boolalpha << arrived_);
    // ROS_INFO_STREAM("Orientation Error: " << euler_pos_err);
    // ROS_INFO_STREAM("Ang Vel Error: " << -1 * euler_fbk_vel.transpose());
    // ROS_INFO_STREAM("Target Position: " << euler_target.transpose());
    // ROS_INFO_STREAM("Position Error: " << position_er

}
void TaskPlanner::check_arrived(){
    
    
    double position_err = (eig_cur_pos - eig_target).norm();
    double orientation_err = quat_ref.angularDistance(quat_fbk);
    if ( position_err< 0.01){
        if (orientation_err <0.1 ){
            if(!arrived_){
                // for (int i = 0; i<7;i++){
                //     eig_arrived_torque(i) = eig_cmd_torque(i);
                // }
                ROS_INFO_STREAM("Arrived!");
                ROS_INFO_STREAM("Orientation Error = " << orientation_err);
                ROS_INFO_STREAM("Position Error = " << position_err);
            }
            arrived_ = true;
        }
        else{
            arrived_ = false;
        } 
    }
    else{
        arrived_ = false;
    }

    ROS_INFO_STREAM("Position Error: " << position_err);
    ROS_INFO_STREAM("Orientation Error: " << orientation_err);
}

void TaskPlanner::compute_field(){
    // All translations
    // Get eig_cur_pos in task space
    fwd_kin(); 
    
    // Get feedback velocity 3x1
    eig_fbk_vel = jacobian_local_translation * eig_joint_vel_;

    // Get feedback angular velocity
    euler_fbk_vel = (jacobian_local_world * eig_joint_vel_).bottomRows(3);
    // All in task space, all reference velocities
    eig_des_vel = k_att_ * (eig_target - eig_cur_pos);

    // Normalize linear velocity
    if (eig_des_vel.norm() > max_lin_vel){
        eig_des_vel= eig_des_vel/eig_des_vel.norm() * max_lin_vel;
    }

    eig_des_pos = eig_cur_pos + 2.0*eig_des_vel/pub_rate_;

    // Compute desired orientations
    // euler_des_pos = pinocchio::rpy::matrixToRpy(ori_ref_mat);
    ori_des_mat = ori_fbk_mat.inverse() * ori_ref_mat;
    euler_pos_err = pinocchio::rpy::matrixToRpy(ori_des_mat);

    // euler_vel_err = ori_fbk_mat * pinocchio::log
    // euler_des_pos = ori_fbk_mat * euler_des_pos;

    pose_ref_ = pinocchio::SE3(ori_ref_mat,eig_des_pos);

    pose_err_ = data.oMi[JOINT_ID].inverse() * pose_ref_;
    pose_err_vec_local = pinocchio::log6(pose_err_).toVector();
    euler_vel_ref = ori_fbk_mat * pose_err_vec_local.bottomRows(3);

    pinocchio::quaternion::assignQuaternion(quat_ref,ori_ref_mat);
    pinocchio::quaternion::assignQuaternion(quat_fbk,ori_fbk_mat);
    quat_err = quat_ref * quat_fbk.conjugate();
    euler_pos_err = pinocchio::rpy::matrixToRpy(quat_err.toRotationMatrix());

}



void TaskPlanner::compute_all_jacob(){
    

    pinocchio::computeAllTerms(model, data, eig_joint_ang_, eig_joint_vel_);
	pinocchio::getJointJacobian(model, data, JOINT_ID, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_local_world);
    
    jacobian_local_translation = jacobian_local_world.topRows(3);
    jacobian_local_pseudo = jacobian_local_world.transpose() * (jacobian_local_world * jacobian_local_world.transpose()).inverse();

    pinocchio::computeJointJacobiansTimeVariation(model, data, eig_joint_ang_, eig_joint_vel_);
	pinocchio::getJointJacobianTimeVariation(model, data, JOINT_ID, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_dot) ;
    jacobian_dot_translation = jacobian_dot.topRows(3);

    jacobian_nullspace = Eigen::MatrixXd::Identity(7,7) - (jacobian_local_translation.transpose()*(jacobian_local_translation*data.M.inverse()*jacobian_local_translation.transpose()).inverse()*jacobian_local_translation*data.M.inverse());
}


void TaskPlanner::update(){

    compute_all_jacob(); // Compute all relevant jacobians required for each iteration

    compute_field(); // Compute potential field

    stack();

    compute_inv_dyn(); // Compute desired joint_position 

    cmd_torque_msg_.data = des_torque_;
    torque_pub_.publish(cmd_torque_msg_);
    
}

void TaskPlanner::fwd_kin(){
    pinocchio::forwardKinematics(model,data,eig_joint_ang_,eig_joint_vel_);
    eig_cur_pos = data.oMi[JOINT_ID].translation();

    ori_fbk_mat =  data.oMi[JOINT_ID].rotation();
    euler_fbk = pinocchio::rpy::matrixToRpy(ori_fbk_mat);
    // euler_fbk = pinocchio::log3(ori_fbk_mat);
    

    // ROS_INFO_STREAM("Feedback Orientation: " << ori_fbk_mat);
}

bool TaskPlanner::ready(){
    if(ros::Time::now().toSec() == 0){
        return false;
    }
    if(cur_joint_pos_[0] > 90.0){
        return false;
    }
    return true;
}
bool TaskPlanner::readParamaters(){
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/hand/position", target_)) {
		ROS_INFO("Can't read target hand positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/hand/orientation", target_orientations)) {
		ROS_INFO("Can't read target hand orientations");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/joint/positions", target_joints)) {
		ROS_INFO("Can't read target joint positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_)) {
		ROS_INFO("Can't read urdf file name");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/linear/max_velocity", max_lin_vel)) {
		ROS_INFO("Can't read max velocity");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_att", k_att_)) {
		ROS_INFO("Can't read k_att");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kp_field", Kp)) {
		ROS_INFO("Can't read kp");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kd_field", Kd)) {
		ROS_INFO("Can't read kd");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_acc", k_acc_)) {
		ROS_INFO("Can't read k_acc_");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kp_null", Kp_null)) {
		ROS_INFO("Can't read K null");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kp_ori", Kp_ori)) {
		ROS_INFO("Can't read Kp orientation");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kd_ori", Kd_ori)) {
		ROS_INFO("Can't read Kd orientation");
		return false;
	}
    ROS_INFO("Target Hand Position: [%f,%f,%f]", target_[0], target_[1], target_[2]);
    ROS_INFO("Target Hand Orientation: [%f,%f,%f]", target_orientations[0], target_orientations[1], target_orientations[2]);
    ROS_INFO_STREAM("Publishing at: " << pub_rate_ << " Hz");
    return true;

}