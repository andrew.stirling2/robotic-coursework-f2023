// #pragma GCC diagnostic push
// #pragma GCC diagnostic ignored "-Wignored-attributes"
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
#include <pinocchio/math/rpy.hpp>
#include <pinocchio/math/quaternion.hpp>
// #pragma GCC diagnostic pop
#include <Eigen/Dense>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <cmath>
#include <highlevel_controller/Controller.hpp>
#include <highlevel_msgs/PoseCommandFeedback.h>

Controller::Controller(ros::NodeHandle& nh) : node_handle_(nh), arrived_(false), cur_time(0),T(5){
    robot_sub_= node_handle_.subscribe("/gen3/joint_states", 1, &Controller::robot_sub_callback,this);
    
    pose_sub_ = node_handle_.subscribe("/gen3/reference/pose",1,&Controller::pose_sub_cb,this);

    twist_sub_ = node_handle_.subscribe("/gen3/reference/twist",1,&Controller::twist_sub_cb,this);

    time_sub = node_handle_.subscribe("/gen3/reference/time",1,&Controller::time_cb,this);

    status_sub = node_handle_.subscribe("/gen3/reference/success",1,&Controller::status_cb,this);
    
    cmd_pub_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/gen3/joint_group_position_controller/command",1);

    des_ang_pos_.resize(7,0.0);
    cur_joint_pos_.resize(8,100.0);
    cur_joint_vel_.resize(8,0.0);
    time_array.resize(2,0.0);
    
    vel_limits_[0] = 1.3963;
    vel_limits_[1] = 1.3963;
    vel_limits_[2] = 1.3963;
    vel_limits_[3] = 1.3963;
    vel_limits_[4] = 1.2218;
    vel_limits_[5] = 1.2218;
    vel_limits_[6] = 1.2218;

    jacobian_local_world = Eigen::MatrixXd::Zero(6, 7);
    jacobian_local_translation = Eigen::MatrixXd::Zero(3,7);
    jacobian_local_pseudo = Eigen::MatrixXd::Zero(7,3);
    jacobian_full_pseudo = Eigen::MatrixXd::Zero(7,6);


    eig_target_joint = Eigen::VectorXd::Zero(7);
    eig_joint_ang = Eigen::VectorXd::Zero(7);
    eig_joint_vel = Eigen::VectorXd::Zero(7);
    eig_des_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_des_joint_vel_ = Eigen::VectorXd::Zero(7);
    eig_des_pos = Eigen::Vector3d::Zero();
    eig_des_pos(0) = 1000;
    eig_des_vel = Eigen::Vector3d::Zero();
    eig_null_joint = Eigen::VectorXd::Zero(7);
    eig_full_vel = Eigen::VectorXd::Zero(6);

    if(readParameters()){
        ROS_INFO("[Controller] Succesfully Read Parameters!");
        pinocchio::urdf::buildModel(urdf_file_, model, false);
        data = pinocchio::Data(model);
    }

    for(int i =0;i<7;i++){
        eig_target_joint(i) = target_joints[i];
    }

    ROS_INFO_STREAM("k_ori_vel: "<<k_ori_vel << " k_joint_vel: "<<k_joint_vel);
    ROS_INFO_STREAM("k_null: "<<k_null << " k_att: "<< k_att);
    ROS_INFO("[Controller] Succesfully connected with Controller");
    
}

void Controller::update(){
    
    compute_vel_fbk(); // Compute desired twist in task space
    compute_inv_kin(); // Compute desired joint_position 
    cmd_ang_pose_.data = des_ang_pos_;
    cmd_pub_.publish(cmd_ang_pose_);
    // if(! arrived_){
    //     cmd_ang_pose_.data = des_ang_pos_;
    //     cmd_pub_.publish(cmd_ang_pose_);
    // }
    
    
}
void Controller::compute_vel_fbk(){
    pinocchio::forwardKinematics(model,data,eig_joint_ang,eig_joint_vel);
    rotmat_fbk = data.oMi[7].rotation();
    pinocchio::quaternion::assignQuaternion(quat_fbk,rotmat_fbk);
    euler_ref_vel = k_ori_vel * pinocchio::rpy::matrixToRpy((quat_ref*quat_fbk.inverse()).normalized().toRotationMatrix());
    
    eig_cur_pos = data.oMi[7].translation();
    eig_des_vel = (eig_des_pos - eig_cur_pos) * pub_rate_ * k_const;

    // if (cur_time > T){
    //     eig_des_vel = k_att*(eig_des_pos - eig_cur_pos);
    //     // ROS_INFO_STREAM("[Controller] Exceeded Time !");
    // }

    if(arrived_){
        eig_des_vel = k_att*(eig_des_pos - eig_cur_pos);
    }

    // if (eig_des_vel.norm()>max_lin_vel){
    //     eig_des_vel = eig_des_vel/eig_des_vel.norm() * max_lin_vel * k_task_vel;
    // }
    // for (int i = 0; i<3;i++){
    //     if (abs(eig_des_vel(i))>max_lin_vel){
    //         eig_des_vel(i) = max_lin_vel* eig_des_vel(i)/ abs(eig_des_vel(i));
    //     }
    // }
    
    eig_full_vel(0) = eig_des_vel(0);
    eig_full_vel(1) = eig_des_vel(1);
    eig_full_vel(2) = eig_des_vel(2);

    eig_full_vel(3) = euler_ref_vel(0);
    eig_full_vel(4) = euler_ref_vel(1);
    eig_full_vel(5) = euler_ref_vel(2);

    // ROS_INFO_STREAM("[Controller] Des Pos: "<< eig_des_pos.transpose());
    // ROS_INFO_STREAM("[Controller] Des Full Vel: "<< eig_full_vel.transpose());
    
}
void Controller::compute_inv_kin(){

    pinocchio::computeAllTerms(model, data, eig_joint_ang, eig_joint_vel);
    pinocchio::getJointJacobian(model, data, 7, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_local_world);
    
    jacobian_local_translation = jacobian_local_world.topRows(3);
    jacobian_local_pseudo = jacobian_local_translation.transpose() * (jacobian_local_translation * jacobian_local_translation.transpose()).inverse();
    
    jacobian_full_pseudo = jacobian_local_world.transpose() * (jacobian_local_world * jacobian_local_world.transpose()).inverse();
    // eig_des_joint_vel_ = jacobian_local_pseudo * eig_des_vel;
    eig_des_joint_vel_ = jacobian_full_pseudo * eig_full_vel;

    // Eigen::MatrixXd jacobian_nullspace = Eigen::MatrixXd::Identity(7,7) - (jacobian_local_pseudo*jacobian_local_translation);
    Eigen::MatrixXd jacobian_nullspace = Eigen::MatrixXd::Identity(7,7) - (jacobian_full_pseudo*jacobian_local_world);

    eig_null_joint = k_null*(eig_target_joint - eig_joint_ang);

    eig_des_joint_vel_ = eig_des_joint_vel_ + (jacobian_nullspace*eig_null_joint);

    double eig_element;
    for(int i = 0; i<7;i++){
        eig_element = eig_des_joint_vel_(i);

        if(abs(eig_element) > vel_limits_[i]){
            eig_des_joint_vel_(i) = eig_element/abs(eig_element)*vel_limits_[i];
            // ROS_INFO("Exceeded velocity limits");
        }
        eig_des_joint_ang_(i) = eig_joint_ang(i) + k_joint_vel*(eig_des_joint_vel_(i) / pub_rate_);
        des_ang_pos_[i] = eig_des_joint_ang_(i);
    }
    return;
}

void Controller::robot_sub_callback(const sensor_msgs::JointState cur_state){
    cur_joint_pos_ = cur_state.position;
    cur_joint_vel_ = cur_state.velocity;

    for (int i = 0; i<7;i++){
        eig_joint_ang(i) = cur_joint_pos_[i];
        eig_joint_vel(i) = cur_joint_vel_[i];
    }
}
void Controller::pose_sub_cb(const geometry_msgs::Pose ref_pose){
    eig_des_pos(0) = ref_pose.position.x;
    eig_des_pos(1) = ref_pose.position.y;
    eig_des_pos(2) = ref_pose.position.z;

    quat_ref.w() = ref_pose.orientation.w;
    quat_ref.x() = ref_pose.orientation.x;
    quat_ref.y() = ref_pose.orientation.y;
    quat_ref.z() = ref_pose.orientation.z;
    
}

void Controller::twist_sub_cb(const geometry_msgs::Twist ref_twist){
    // eig_des_vel(0) = ref_twist.linear.x;
    // eig_des_vel(1) = ref_twist.linear.y;
    // eig_des_vel(2) = ref_twist.linear.z;
}

void Controller::time_cb(const std_msgs::Float64MultiArray time_msg){
    if(time_msg.data.size() == 2){
        T = time_msg.data[0];
        cur_time = time_msg.data[1];
    }
    // time_msg.data[0] = T_;
	// time_msg.data[1] = t_current;
}
void Controller::status_cb(const std_msgs::Bool success_msg){
    arrived_ = success_msg.data;
    // ROS_INFO("[Controller] Arrived: %s", arrived_ ? "true" : "false");
}
bool Controller::readParameters(){
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/joint/positions", target_joints)) {
		ROS_INFO("Can't read target joint positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_)) {
		ROS_INFO("Can't read urdf file name");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/linear/max_velocity", max_lin_vel)) {
		ROS_INFO("Can't read max velocity");
		return false;
	}
    if ( !node_handle_.getParam("/controller/gains/k_null", k_null)) {
		ROS_INFO("Can't read k_null");
		return false;
	}
    if ( !node_handle_.getParam("/controller/gains/k_att", k_att)) {
		ROS_INFO("Can't read k_att");
		return false;
	}
    if ( !node_handle_.getParam("/controller/gains/k_joint_vel", k_joint_vel)) {
		ROS_INFO("Can't read k_joint_vel");
		return false;
	}
    if ( !node_handle_.getParam("/controller/gains/k_ori_vel", k_ori_vel)) {
		ROS_INFO("Can't read k_ori_vel");
		return false;
	}
    if ( !node_handle_.getParam("/controller/gains/k_task_vel", k_task_vel)) {
		ROS_INFO("Can't read k_task_vel");
		return false;
	}
    if ( !node_handle_.getParam("/controller/gains/k_const", k_const)) {
		ROS_INFO("Can't read k_const");
		return false;
	}

    ROS_INFO_STREAM("Publishing at: " << pub_rate_ << " Hz");
    ROS_INFO_STREAM("URDF File found at: " << urdf_file_);
    return true;

}

bool Controller::ready(){
    if(ros::Time::now().toSec() == 0){
        return false;
    }
    if(cur_joint_pos_[0] > 90.0){
        return false;
    }
    if (eig_des_pos(0)>900){
        return false;
    }

    return true;
}