#include <highlevel_controller/ActionClient.hpp>
#include <highlevel_msgs/PoseCommandAction.h>
#include <ros/ros.h>
#include <Eigen/Dense>

ActionClient::ActionClient(ros::NodeHandle& nh): node_handle_(nh),past_grip_pos(0.0),
    pose_cmd_action_client_("/gen3/action_planner/pose",true),
    grip_cmd_action_client_("/gen3/finger_group_action_controller/gripper_cmd",true){

    if (!readParameters()){
        ROS_INFO_STREAM("[ActionClient] Can't read params");
        ros::requestShutdown();
    }
    ROS_INFO("[ActionClient] Succesfully read parameters, reading into target matrix");
    ROS_INFO_STREAM("[ActionClient] Num Targets: " << NUM_TARGETS_);
    targets_ = Eigen::MatrixXd::Zero(NUM_TARGETS_,9);
    readTargets();
    ROS_INFO_STREAM("[ActionClient] Succesfully read Target Matrix: ");
    ROS_INFO_STREAM(targets_);
    ROS_INFO("[ActionClient] Succesfully connected with client");

}

void ActionClient::update(){
    ROS_INFO_STREAM("[ActionClient::update] Action Client Ready. Waiting for Server...");

    for( int counter = 0;counter<NUM_TARGETS_;counter++){
        if(targets_(counter,7) == -1.0){
            action_goal_.x = targets_(counter,0);
            action_goal_.y = targets_(counter,1);
            action_goal_.z = targets_(counter,2);
            action_goal_.roll = targets_(counter,3);
            action_goal_.pitch = targets_(counter,4);
            action_goal_.yaw = targets_(counter,5);
            action_goal_.T = targets_(counter,6);

            pose_cmd_action_client_.waitForServer();

            pose_cmd_action_client_.sendGoal(action_goal_,
                boost::bind(&ActionClient::doneCallback, this,_1,_2),
                boost::bind(&ActionClient::activeCallback, this),
                boost::bind(&ActionClient::feedbackCallback, this, _1));
                
            ROS_INFO_STREAM("[ActionClient::update] Sent action pose goal. Waiting for the results...") ;
            pose_cmd_action_client_.waitForResult(ros::Duration(30.0));

            if (pose_cmd_action_client_.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO_STREAM("[ActionClient::update] Yay! The end-effector reaches the goal position") ;
            }
        }
        else{
            // Hz (points/s) * time (s) = points
            double dist = targets_(counter,7) - past_grip_pos;
            double points = pub_rate_* targets_(counter,6);
            for(int i = 1;i<=points;i++){
                grip_goal_.command.position = past_grip_pos + ((i/points) * dist);
                grip_goal_.command.max_effort = targets_(counter,8);

                grip_cmd_action_client_.waitForServer();

                grip_cmd_action_client_.sendGoal(grip_goal_,
                    boost::bind(&ActionClient::gripDoneCallback, this,_1,_2),
                    boost::bind(&ActionClient::gripActiveCallback, this),
                    boost::bind(&ActionClient::gripFeedbackCallback, this, _1));

                ROS_INFO_STREAM("[ActionClient::update] Sent grip position: "<< grip_goal_.command.position);
                grip_cmd_action_client_.waitForResult(ros::Duration(targets_(counter,6)));
            }

            // grip_goal_.command.position = targets_(counter,7);
            // grip_goal_.command.max_effort = targets_(counter,8);

            // grip_cmd_action_client_.waitForServer();

            // grip_cmd_action_client_.sendGoal(grip_goal_,
            //     boost::bind(&ActionClient::gripDoneCallback, this,_1,_2),
            //     boost::bind(&ActionClient::gripActiveCallback, this),
            //     boost::bind(&ActionClient::gripFeedbackCallback, this, _1));

            // ROS_INFO_STREAM("[ActionClient::update] Sent grip goal. Waiting for results...");
            // grip_cmd_action_client_.waitForResult(ros::Duration(30.0));
            if (grip_cmd_action_client_.getState()== actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO_STREAM("[ActionClient::update] Gripper has gripped successfully!");
            }
            past_grip_pos = targets_(counter,7);

        }
        
    }
    ROS_INFO_STREAM("[ActionClient::doneCallback] Done, shutdown") ;
	ros::shutdown();
}
void ActionClient::doneCallback(const actionlib::SimpleClientGoalState& state, const highlevel_msgs::PoseCommandResultConstPtr& result){
    ROS_INFO("[ActionClient::doneCallback] Finished in state [%s]", state.toString().c_str());
}
void ActionClient::gripDoneCallback(const actionlib::SimpleClientGoalState& state,const control_msgs::GripperCommandResultConstPtr& result){
    ROS_INFO("[ActionClient::doneCallback] Finished in state [%s]", state.toString().c_str());
}
void ActionClient::activeCallback(){
    ROS_INFO_STREAM("[ActionClient::activeCallback] Action has become active");
}
void ActionClient::gripActiveCallback(){
    ROS_INFO_STREAM("[ActionClient::gripActiveCallback] Gripper action has become active");
}
void ActionClient::feedbackCallback(const highlevel_msgs::PoseCommandFeedbackConstPtr& feedback){
    // ROS_INFO_STREAM("[ActionClient::feedbackCallback] Distance Trans=" << feedback->distance_translation 
    //                     << " ,Distance Ori=" << feedback->distance_orientation << " ,Time=" << feedback->time_elapsed);
}
void ActionClient::gripFeedbackCallback(const control_msgs::GripperCommandFeedbackConstPtr& feedback){

}

bool ActionClient::readParameters(){
    if ( !node_handle_.getParam("/action_list/number_of_targets", NUM_TARGETS_)) {
		ROS_INFO("Can't read number of targets");
		return false;
	}
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish_rate");
		return false;
	}

    return true;
}
ActionClient::~ActionClient(){
}

void ActionClient::readTargets(){
    bool succ;
    std::string action_base_name = "/action_list/action_";
    ROS_INFO_STREAM("Action base name :"<<action_base_name<< " Num targets: "<< NUM_TARGETS_);
    for(int i=0;i<NUM_TARGETS_;i++){
        std::string action_type_name = action_base_name + std::to_string(i) + "/action_type";
        std::string action_type;
        node_handle_.getParam(action_type_name,action_type);
        if(action_type == "pose"){
            std::string trans_name = action_base_name + std::to_string(i) + "/translation";
            std::string ori_name = action_base_name + std::to_string(i) +"/orientation";
            std::string duration_name = action_base_name + std::to_string(i) +"/duration";
            std::vector<double> trans_vec;
            ROS_INFO_STREAM("Trans: "<<trans_name << " Ori: "<<ori_name << " Dur: " << duration_name);
            succ = node_handle_.getParam(trans_name,trans_vec);
            std::vector<double> ori_vec;
            succ = succ && node_handle_.getParam(ori_name,ori_vec);
            double duration;
            succ = succ && node_handle_.getParam(duration_name,duration);
            targets_(i,0) = trans_vec[0];
            targets_(i,1) = trans_vec[1];
            targets_(i,2) = trans_vec[2];
            targets_(i,3) = ori_vec[0];
            targets_(i,4) = ori_vec[1];
            targets_(i,5) = ori_vec[2];
            targets_(i,6) = duration;
            targets_(i,7) = -1;
            targets_(i,8) = -1;
        }
        else{
            std::string position_name = action_base_name + std::to_string(i) + "/position";
            std::string effort_name = action_base_name + std::to_string(i) + "/effort";
            std::string duration_name = action_base_name + std::to_string(i) + "/duration";
            double duration;
            double effort;
            double position;
            succ = node_handle_.getParam(position_name, position);
            succ = node_handle_.getParam(effort_name,effort);
            succ = node_handle_.getParam(duration_name,duration);
            targets_(i,0) = -1.0;
            targets_(i,1) = -1.0;
            targets_(i,2) = -1.0;
            targets_(i,3) = -1.0;
            targets_(i,4) = -1.0;
            targets_(i,5) = -1.0;
            targets_(i,6) = duration;
            targets_(i,7) = position;
            targets_(i,8) = effort;
        }        
    }
    if(succ){
        ROS_INFO_STREAM("Succesfully read Target Matrix: ");
        ROS_INFO_STREAM(targets_);
    }
}
bool ActionClient::ready(){
    if(ros::Time::now().toSec() == 0){
        return false;
    }
    return true;
}