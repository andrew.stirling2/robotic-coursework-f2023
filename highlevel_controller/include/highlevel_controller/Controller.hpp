#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64MultiArray.h>
#include <highlevel_msgs/MoveTo.h>
#include <std_msgs/Bool.h>
#include <highlevel_controller/ActionClient.hpp>

class Controller{
    public: 

        Controller(ros::NodeHandle& nh);

        void robot_sub_callback(const sensor_msgs::JointState cur_state);

        void pose_sub_cb(const geometry_msgs::Pose ref_pose);

        void twist_sub_cb(const geometry_msgs::Twist ref_twist);

        void time_cb(const std_msgs::Float64MultiArray time_msg);

        void status_cb(const std_msgs::Bool status);
        
        void compute_inv_kin();

        void compute_vel_fbk();

        void update();

        bool ready();

    protected:

        bool readParameters();

    private:

        ros::NodeHandle& node_handle_;
        ros::Subscriber robot_sub_;
        ros::Subscriber pose_sub_;
        ros::Subscriber twist_sub_;
        ros::Subscriber time_sub;
        ros::Subscriber status_sub;
        ros::Publisher cmd_pub_;
        ros::Subscriber action_fbk_sub;


        // Params
        std::vector<double> target_joints;
        double pub_rate_;
        std::string urdf_file_;
        double max_lin_vel;
        double vel_limits_[7];
        // Config Params
        double k_null;
        double k_att;
        double k_joint_vel;
        double k_ori_vel;
        double k_task_vel;

        // Pinocchio Vars
        pinocchio::Model model;
        pinocchio::Data data;
        pinocchio::SE3 pose_ref_;
        pinocchio::SE3 pose_err_;
        Eigen::VectorXd pose_err_vec_local;
        Eigen::VectorXd pose_err_vec_global; 

        // Eigen
        Eigen::MatrixXd jacobian_local_world;
        Eigen::MatrixXd jacobian_local_translation;
        Eigen::MatrixXd jacobian_local_pseudo;
        Eigen::MatrixXd jacobian_full_pseudo;
        Eigen::MatrixXd jacobian_nullspace;
        Eigen::Vector3d eig_des_pos;
        Eigen::Vector3d eig_des_vel;
        Eigen::VectorXd eig_des_joint_ang_;
        Eigen::VectorXd eig_des_joint_vel_;
        Eigen::VectorXd eig_target_joint;
        Eigen::VectorXd eig_null_joint;
        Eigen::Vector3d eig_cur_pos;
        Eigen::VectorXd eig_full_vel;
        Eigen::VectorXd eig_joint_ang;
        Eigen::VectorXd eig_joint_vel;

        Eigen::Vector3d euler_ref;
        Eigen::Vector3d euler_ref_vel;
        Eigen::Quaterniond quat_ref;
        Eigen::Quaterniond quat_fbk;
        Eigen::Matrix3d rotmat_fbk;

        // To Publish
        std::vector<double> des_ang_pos_;
        std_msgs::Float64MultiArray cmd_ang_pose_;

        // Other Vars
        bool arrived_;
        std::vector<double> cur_joint_pos_;
        std::vector<double> cur_joint_vel_;
        double T;
        double cur_time;
        std::vector<double> time_array;
        double k_const;
};