#include <Eigen/Dense>
#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <highlevel_msgs/PoseCommandAction.h>
#include <control_msgs/GripperCommandAction.h>

class ActionClient {
    public:
        ActionClient(ros::NodeHandle& nh);

        virtual ~ActionClient();

        void update();

        bool ready();

    protected:

        void activeCallback();
        void feedbackCallback(const highlevel_msgs::PoseCommandFeedbackConstPtr& feedback);
        void doneCallback(const actionlib::SimpleClientGoalState& state, const highlevel_msgs::PoseCommandResultConstPtr& result);

        void gripActiveCallback();
        void gripFeedbackCallback(const control_msgs::GripperCommandFeedbackConstPtr& feedback);
        void gripDoneCallback(const actionlib::SimpleClientGoalState& state,const control_msgs::GripperCommandResultConstPtr& result);

    private:

        bool readParameters();
        void readTargets();

        ros::NodeHandle& node_handle_;

        ros::Subscriber fbk_sub_;

        ros::ServiceServer service_server_;
        highlevel_msgs::PoseCommandGoal action_goal_;
        control_msgs::GripperCommandGoal grip_goal_;
        actionlib::SimpleActionClient<highlevel_msgs::PoseCommandAction> pose_cmd_action_client_;
        actionlib::SimpleActionClient<control_msgs::GripperCommandAction> grip_cmd_action_client_;
        int NUM_TARGETS_;

        Eigen::MatrixXd targets_;

        int counter_;
        int cleaning;

        int pub_rate_;
        double past_grip_pos;

};