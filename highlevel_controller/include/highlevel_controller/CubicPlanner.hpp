#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <highlevel_msgs/MoveTo.h>

class CubicPlanner{
    public:

        CubicPlanner(ros::NodeHandle& nh);

        bool service_callback(highlevel_msgs::MoveTo::Request  &req,
				 	 highlevel_msgs::MoveTo::Response &res);

        bool service_callback_ori(highlevel_msgs::MoveTo::Request  &req,
				 	 highlevel_msgs::MoveTo::Response &res);
        void sub_callback(const sensor_msgs::JointState cur_state);

        void compute_traj();

        void compute_ori();

        void compute_inv_kin();

        void compute_start_pos();

        void update();

        bool ready();

    protected:

        bool readParamaters();

    private:
        ros::NodeHandle& node_handle_;
        ros::Subscriber sub_;
        ros::ServiceServer service_;
        ros::ServiceServer service_ori;
        ros::Publisher ang_pub_;

        // Params
        std::vector<double> target_;
        std::vector<double> target_orientations;
        std::vector<double> target_joints;
        double pub_rate_;
        std::string urdf_file_;
        double max_lin_vel;
        // To Publish
        std::vector<double> des_ang_pos_;
        std_msgs::Float64MultiArray cmd_ang_pose_;


        // Pinocchio Vars
        pinocchio::Model model;
        pinocchio::Data data;
        Eigen::Vector3d eig_start_pos;
        Eigen::VectorXd eig_start_joint_ang_;
        Eigen::VectorXd eig_joint_ang_;
        Eigen::VectorXd eig_joint_vel_;
        Eigen::Vector3d eig_des_pos;
        Eigen::Vector3d eig_des_vel;
        Eigen::MatrixXd jacobian_local_world;
        Eigen::MatrixXd jacobian_local_translation;
        Eigen::MatrixXd jacobian_local_pseudo;
        Eigen::MatrixXd jacobian_full_pseudo;
        Eigen::MatrixXd jacobian_nullspace;
        Eigen::VectorXd eig_des_joint_ang_;
        Eigen::VectorXd eig_des_joint_vel_;
        Eigen::Vector3d eig_target;
        Eigen::VectorXd eig_target_joint;
        Eigen::VectorXd eig_null_joint;
        Eigen::Vector3d eig_cur_pos;
        Eigen::VectorXd eig_full_vel;

        Eigen::VectorXd eig_arrived_joint_pos;
        const int JOINT_ID;

        // Service Stuff
        
        double T_;
        bool has_start_pos_;
        double k_null;
        double k_att;
        double k_joint_vel;
        double k_ori_vel;
        int debug;


        // Vars
        std::vector<double> cur_joint_pos_;
        std::vector<double> cur_joint_vel_;
        double t_initial_;
        double offset_time_;
        double cur_time_;
        double time_coeff_;
        double vel_limits_[7];
        bool arrived_;
        bool move_ori_called;
        double max_ang_vel;

        // Orientations
        Eigen::Matrix3d rotmat_start;
        Eigen::Matrix3d rotmat_ref;
        Eigen::Matrix3d rotmat_fbk;
        Eigen::Vector3d euler_target;
        Eigen::Vector3d euler_ref;
        Eigen::Vector3d euler_ref_vel;
        Eigen::Vector3d euler_fbk;
        Eigen::Matrix3d rotmat_target;
        Eigen::Quaterniond quat_start;
        Eigen::Quaterniond quat_target;
        Eigen::Quaterniond quat_ref;
        Eigen::Quaterniond quat_fbk;

        pinocchio::SE3 pose_ref_;
        pinocchio::SE3 pose_err_;
        Eigen::VectorXd pose_err_vec_local;
        Eigen::VectorXd pose_err_vec_global; 




};