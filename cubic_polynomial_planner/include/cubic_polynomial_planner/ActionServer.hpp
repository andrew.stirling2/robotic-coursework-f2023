#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <highlevel_msgs/PoseCommandAction.h>
#include <highlevel_msgs/MoveTo.h>
#include <gazebo_msgs/ModelStates.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/JointState.h>
#include <Eigen/Dense>
#include <vector>

class ActionServer{
    public:
        ActionServer(ros::NodeHandle& nh);

        virtual ~ActionServer();

        void update();

        void computeStartPosition();

        void computeCubicPolynomial();

        void computeFeedback();

        void publishRefs();

        void publishTargets();

        bool ready();

        bool readParameters();
        void subCallback(const sensor_msgs::JointState cur_state);
        void poseCommandCallback(const highlevel_msgs::PoseCommandGoalConstPtr& goal);
        void feedbackCallback(const gazebo_msgs::ModelStates& fbk_pose_msg);

    private:
        // ROS Stuff
        ros::NodeHandle& node_handle_;
        ros::Subscriber feedback_subscriber;
        ros::Subscriber joint_state_sub;
        ros::Publisher pose_publisher;
        ros::Publisher twist_publisher;
        ros::Publisher time_publisher;
        ros::Publisher success_publisher;
        
        geometry_msgs::Pose pose_msg;
        geometry_msgs::Twist twist_msg;
        std_msgs::Bool succ_msg;
        
        // Action Stuff
        highlevel_msgs::PoseCommandGoal action_goal;
        highlevel_msgs::PoseCommandFeedback action_feedback;
        actionlib::SimpleActionServer<highlevel_msgs::PoseCommandAction> cmd_action_server;
        
        ros::Rate loop_rate_;

        // Feedback
        std::vector<double> cur_joint_pos_;
        std::vector<double> cur_joint_vel_;
        // Action Vars
        
        // Vars
        double t_current;
        double t_start;
        double t_coeff_pos;
        double t_coeff_vel;
        double T_;
        bool has_start_pos;
        bool success;
        bool pose_cmd_called;
        std::string urdf_file_;
        
        //Eigen Stuff
        Eigen::VectorXd fbk_joint_ang_;
        Eigen::VectorXd fbk_joint_vel_;
        // Pinocchio Vars
        pinocchio::Model model;
        pinocchio::Data data;
        Eigen::Vector3d eig_start_pos;
        Eigen::VectorXd eig_start_joint_ang_;
        Eigen::Vector3d eig_des_pos;
        Eigen::Vector3d eig_des_vel;
        Eigen::MatrixXd jacobian_local_world;
        Eigen::MatrixXd jacobian_local_translation;
        Eigen::MatrixXd jacobian_local_pseudo;
        Eigen::MatrixXd jacobian_full_pseudo;
        Eigen::MatrixXd jacobian_nullspace;
        Eigen::VectorXd eig_des_joint_ang_;
        Eigen::VectorXd eig_des_joint_vel_;
        Eigen::Vector3d eig_target;
        Eigen::VectorXd eig_target_joint;
        Eigen::VectorXd eig_null_joint;
        Eigen::Vector3d eig_cur_pos;
        Eigen::VectorXd eig_full_vel;

        // Orientations
        Eigen::Matrix3d rotmat_start;
        Eigen::Matrix3d rotmat_ref;
        Eigen::Matrix3d rotmat_fbk;
        Eigen::Vector3d euler_target;
        Eigen::Vector3d euler_ref;
        Eigen::Vector3d euler_ref_vel;
        Eigen::Vector3d euler_fbk;
        Eigen::Matrix3d rotmat_target;
        Eigen::Quaterniond quat_start;
        Eigen::Quaterniond quat_target;
        Eigen::Quaterniond quat_ref;
        Eigen::Quaterniond quat_fbk;

        // Params
        double max_lin_vel;
        double pub_rate_;
};