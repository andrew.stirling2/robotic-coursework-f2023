#include <ros/ros.h>
#include <highlevel_msgs/MoveTo.h>
#include <geometry_msgs/Pose.h>

class PosePlanner {
    public:
        // Constructor
        PosePlanner(ros::NodeHandle& nh);
        // Computes new pose
        // Assumes valid inputs, callback handles bad inputs
        bool service_callback(highlevel_msgs::MoveTo::Request  &req,
				 	 highlevel_msgs::MoveTo::Response &res);
        
        void sub_callback(const geometry_msgs::Pose pose);

        bool compute_trans(); 

        void update();

        bool can_serve();

        geometry_msgs::Pose get_pose();


    private:
        // ROS Stuff
        ros::NodeHandle& node_handle_ ;
        ros::ServiceServer service_;
        ros::Subscriber subscriber_;
        ros::Publisher publisher_;

        // Service Stuff
        double x_f_;
        double y_f_;
        double z_f_;
        double t_initial_;
        double T_;
        geometry_msgs::Pose des_pos_;
        geometry_msgs::Pose fbk_pos_;
        bool has_start_pos_;
        geometry_msgs::Pose start_pos_;
        bool service_success_;

};
