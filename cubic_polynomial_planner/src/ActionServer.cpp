#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
#include <pinocchio/math/rpy.hpp>
#include <pinocchio/math/quaternion.hpp>

#include <Eigen/Dense>
#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <vector>
#include <cmath>
#include <highlevel_msgs/MoveTo.h>
#include <highlevel_msgs/PoseCommandGoal.h>
#include <cubic_polynomial_planner/ActionServer.hpp>


ActionServer::ActionServer(ros::NodeHandle& node_handle): node_handle_(node_handle),loop_rate_(500),success(false),
cmd_action_server(node_handle_,"/gen3/action_planner/pose",boost::bind(&ActionServer::poseCommandCallback, this, _1 ), false){

    if ( !readParameters() ) {
		ROS_ERROR("[Server] Could not read parameters");
		ros::requestShutdown();
	}
	ROS_INFO("[ActionServer] Successfully read parameters");
	feedback_subscriber = node_handle_.subscribe("/gazebo/model_states", 1, &ActionServer::feedbackCallback,this);
	joint_state_sub = node_handle_.subscribe("/gen3/joint_states", 1, &ActionServer::subCallback,this);
	pose_publisher = node_handle.advertise<geometry_msgs::Pose>("/gen3/reference/pose",1);
	twist_publisher = node_handle.advertise<geometry_msgs::Twist>("/gen3/reference/twist",1);
	time_publisher = node_handle.advertise<std_msgs::Float64MultiArray>("/gen3/reference/time",1);
	success_publisher = node_handle.advertise<std_msgs::Bool>("/gen3/reference/success",1);

	ROS_INFO("[Action Server] Set up subs/pubs");
	pinocchio::urdf::buildModel(urdf_file_, model, false);
	data = pinocchio::Data(model);

	cur_joint_pos_.resize(8,100.0);
    cur_joint_vel_.resize(8,0.0);
	fbk_joint_ang_ = Eigen::VectorXd::Zero(7);
    fbk_joint_vel_ = Eigen::VectorXd::Zero(7);
	euler_target = Eigen::Vector3d::Zero();
	eig_start_pos = Eigen::Vector3d::Zero();
	

	cmd_action_server.start();
	ROS_INFO_STREAM("[ActionServer] Connected with server");

}

void ActionServer::subCallback(const sensor_msgs::JointState cur_state){
	// ROS_INFO("[ActionServer] Reached subCallback()");
	cur_joint_pos_ = cur_state.position;
    cur_joint_vel_ = cur_state.velocity;
	// ROS_INFO_STREAM("[ActionServer] Read into std vectors");
    for (int i = 0; i<7;i++){
        fbk_joint_ang_(i) = cur_joint_pos_[i];
        fbk_joint_vel_(i) = cur_joint_vel_[i];
    }
	// ROS_INFO_STREAM("[ActionServer] Read into eigen vectors");
}
void ActionServer::feedbackCallback(const gazebo_msgs::ModelStates& fbk_pose_msg) {

	if ( cmd_action_server.isActive() ) {
		// ROS_INFO_STREAM("[ActionServer::feedbackCallback] pose= "
		// 		<< fbk_pose_msg.pose[1].position.x << ", "
		// 		<< fbk_pose_msg.pose[1].position.y << ", "
		// 		<< fbk_pose_msg.pose[1].position.z );
	}
}
void ActionServer::update(){
	// ROS_INFO_STREAM("Reach update");
	if (pose_cmd_called){
		computeCubicPolynomial();
		
	}
	
	succ_msg.data = success;
	success_publisher.publish(succ_msg);

	// ROS_INFO("[ActionServer] Reached update");
	// ROS_INFO_STREAM("[ActionServer::update] fbk trans = " << action_feedback.distance_translation);
}
void ActionServer::poseCommandCallback(const highlevel_msgs::PoseCommandGoalConstPtr& goal){
	ROS_INFO_STREAM("[ActionServer::poseCommandCallback] Received Goal: x=" 
			<< goal->x << ", y=" << goal->y << ", z= " << goal->z << " , roll="
			<< goal->roll << ", pitch=" << goal->pitch << ", yaw= " << goal->yaw << " ,T=" <<goal->T);
	
	euler_target(0) = goal->roll;
	euler_target(1) = goal->pitch;
	euler_target(2) = goal->yaw;

	eig_target(0) = goal->x;
	eig_target(1) = goal->y;
	eig_target(2) = goal->z;

	T_ = goal->T;

	has_start_pos = false;
	success = false;
	
	pose_cmd_called = false;

	while (!success){
		pose_cmd_called = true;
		// update();
		computeFeedback();
		// ROS_INFO("[ActionServer] Finished Feedback loop");
		cmd_action_server.publishFeedback(action_feedback);
		// ROS_INFO_STREAM("[ActionServer::poseCommandCallback] feedback trans=" << action_feedback.distance_translation);
		loop_rate_.sleep();
	}
	// ROS_INFO("[ActionServer] Finished Feedback loop");
	cmd_action_server.setSucceeded();
	succ_msg.data = success;
	success_publisher.publish(succ_msg);
	ROS_INFO_STREAM("[ActionServer::poseCommandCallback] Action ompleted in "<< t_current << " s");

}
void ActionServer::computeCubicPolynomial(){
	if(!has_start_pos){
		computeStartPosition();
	}
	t_current = ros::Time::now().toSec() - t_start;

	if(t_current < T_){
		t_coeff_pos= (3*pow(t_current,2)/pow(T_,2)) - (2*pow(t_current,3)/pow(T_,3));
		eig_des_pos = eig_start_pos + (t_coeff_pos*(eig_target - eig_start_pos));
		t_coeff_vel = (6*t_current/pow(T_,2)) - (6*pow(t_current,2)/pow(T_,3));
		eig_des_vel = t_coeff_vel*(eig_target - eig_start_pos);
		quat_ref = quat_start.slerp(t_coeff_pos,quat_target);
		publishRefs();
	}
	else{
		publishTargets();
	}
	

}
void ActionServer::computeStartPosition(){
	pinocchio::forwardKinematics(model,data,fbk_joint_ang_,fbk_joint_vel_);
	pinocchio::SE3 pose_now = data.oMi[7];
	// Get Translation
	eig_start_pos = pose_now.translation();
	// Get Orientation
	rotmat_start = pose_now.rotation();
	pinocchio::quaternion::assignQuaternion(quat_start,rotmat_start);
	rotmat_target = pinocchio::rpy::rpyToMatrix(euler_target(0),euler_target(1),euler_target(2));
	pinocchio::quaternion::assignQuaternion(quat_target,rotmat_target);
    pinocchio::quaternion::assignQuaternion(quat_start,rotmat_start);
	// Start Time
	t_start = ros::Time::now().toSec();
	has_start_pos = true;
}
void ActionServer::computeFeedback(){
	pinocchio::forwardKinematics(model,data,fbk_joint_ang_,fbk_joint_vel_);
	eig_cur_pos = data.oMi[7].translation();
	rotmat_fbk = data.oMi[7].rotation();
	pinocchio::quaternion::assignQuaternion(quat_fbk,rotmat_fbk);
	double position_err = (eig_target - eig_cur_pos).norm();
	double ori_err = quat_fbk.angularDistance(quat_target);
	
	action_feedback.distance_translation = position_err;
	action_feedback.distance_orientation = ori_err;
	action_feedback.time_elapsed = t_current;
	if(position_err<0.01){
		if(ori_err<0.08){
			success = true;
			succ_msg.data = success;
		}
	}
}
void ActionServer::publishRefs(){
	// Read in pose to message
	pose_msg.position.x = eig_des_pos(0);
	pose_msg.position.y = eig_des_pos(1);
	pose_msg.position.z = eig_des_pos(2);

	pose_msg.orientation.x = quat_ref.x();
	pose_msg.orientation.y = quat_ref.y();
	pose_msg.orientation.z = quat_ref.z();
	pose_msg.orientation.w = quat_ref.w();
	// Read in twist to message
	twist_msg.linear.x = eig_des_vel(0);
	twist_msg.linear.y = eig_des_vel(1);
	twist_msg.linear.z = eig_des_vel(2);
	twist_msg.angular.x = 0;
	twist_msg.angular.y = 0;
	twist_msg.angular.z = 0;
	
	std_msgs::Float64MultiArray time_msg;
	time_msg.data.push_back(T_);
	time_msg.data.push_back(t_current);

	// Publish
	pose_publisher.publish(pose_msg);
	twist_publisher.publish(twist_msg);	
	time_publisher.publish(time_msg);
}
void ActionServer::publishTargets(){
	// Read in pose to message
	pose_msg.position.x = eig_target(0);
	pose_msg.position.y = eig_target(1);
	pose_msg.position.z = eig_target(2);

	pose_msg.orientation.x = quat_target.x();
	pose_msg.orientation.y = quat_target.y();
	pose_msg.orientation.z = quat_target.z();
	pose_msg.orientation.w = quat_target.w();
	// Read in twist to message
	twist_msg.linear.x = 0;
	twist_msg.linear.y = 0;
	twist_msg.linear.z = 0;
	twist_msg.angular.x = 0;
	twist_msg.angular.y = 0;
	twist_msg.angular.z = 0;
	
	std_msgs::Float64MultiArray time_msg;
	time_msg.data.push_back(T_);
	time_msg.data.push_back(t_current);

	// Publish
	pose_publisher.publish(pose_msg);
	twist_publisher.publish(twist_msg);	
	time_publisher.publish(time_msg);
}
bool ActionServer::readParameters(){
	if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
	if ( !node_handle_.getParam("/gen3/linear/max_velocity", max_lin_vel)) {
		ROS_INFO("Can't read max velocity");
		return false;
	}
	if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_)) {
		ROS_INFO("Can't read urdf file name");
		return false;
	}

	ROS_INFO_STREAM("Max Linear Velocity = " << max_lin_vel);
	ROS_INFO_STREAM("Publishing at "<< pub_rate_);
	return true;
}
ActionServer::~ActionServer(){
}

bool ActionServer::ready(){
	if(ros::Time::now().toSec() == 0){
        return false;
    }
    if(cur_joint_pos_[0] > 90.0){
        return false;
    }
    return true;
}
