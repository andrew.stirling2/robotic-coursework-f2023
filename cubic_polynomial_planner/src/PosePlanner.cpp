#include <ros/ros.h>
#include <Eigen/Dense>
#include <cubic_polynomial_planner/PosePlanner.hpp>
#include <highlevel_msgs/MoveTo.h>
#include <geometry_msgs/Pose.h>

PosePlanner::PosePlanner(ros::NodeHandle& nh) : node_handle_(nh), has_start_pos_(false), service_success_(false) {
    service_ = node_handle_.advertiseService("pose_planner/move_to", &PosePlanner::service_callback,this);

    subscriber_ = node_handle_.subscribe("/firefly/ground_truth/pose", 5, &PosePlanner::sub_callback,this);

    publisher_ = node_handle_.advertise<geometry_msgs::Pose>("/firefly/command/pose", 5);
}
void PosePlanner::sub_callback(const geometry_msgs::Pose pose){
    fbk_pos_ = pose;
}

bool PosePlanner::service_callback(highlevel_msgs::MoveTo::Request  &req,
				 	                highlevel_msgs::MoveTo::Response &res){
    
    if (req.z <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to z position must be greater than 0");
        service_success_ = false;
        return false;
    }
    if (req.T <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to period T must be greater than 0");
        service_success_ = false;
        return false;
    }
    x_f_ = req.x;
    y_f_ = req.y;
    z_f_ = req.z;
    T_ = req.T;
    t_initial_ = ros::Time::now().toSec(); // get current time in seconds
    service_success_ = true;
    res.can_execute = true;
    has_start_pos_ = false;
    return true;

}

bool PosePlanner::compute_trans(){
    double t_cur =  ros::Time::now().toSec(); // get current time in seconds
    double adjusted_time = t_cur - t_initial_;
    // Reached final position
    if (adjusted_time > 1.0*T_){
        // Reset service and start pos flags
        service_success_ = false ;
        has_start_pos_ = false;
        ROS_INFO_STREAM("Completed movement in " << adjusted_time << " seconds");
        return false;
    }

    if (! has_start_pos_){
        // Assign start position of rotor
        start_pos_ = fbk_pos_ ;
        has_start_pos_ = true;
    }
    double time_coeff;
    // time_coeff = 3*adjusted_time*adjusted_time / (1.0 * T_ * T_);
    // time_coeff = time_coeff - ( 2* adjusted_time * adjusted_time * adjusted_time / (1.0 * T_ * T_ * T_));
    time_coeff = (3*adjusted_time*adjusted_time / (1.0*T_*T_)) - (2*adjusted_time*adjusted_time*adjusted_time/(1.0*T_*T_*T_));
    //TODO: Implement trajectory function 
    des_pos_.position.x = start_pos_.position.x + (time_coeff*((1.0*x_f_) - start_pos_.position.x));
    des_pos_.position.y = start_pos_.position.y + (time_coeff*((1.0*y_f_) - start_pos_.position.y));
    des_pos_.position.z = start_pos_.position.z + (time_coeff*((1.0*z_f_) - start_pos_.position.z));
    des_pos_.orientation = start_pos_.orientation;

    return true;
}

bool PosePlanner::can_serve(){
    return service_success_;
}

geometry_msgs::Pose PosePlanner::get_pose(){
    return des_pos_;
}

void PosePlanner::update(){
    if (service_success_){
        if (compute_trans()){
            publisher_.publish(des_pos_);
        }
    }
}