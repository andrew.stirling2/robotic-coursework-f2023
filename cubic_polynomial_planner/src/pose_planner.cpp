#include <Eigen/Dense>
#include <ros/ros.h>
#include <highlevel_msgs/MoveTo.h>
#include <geometry_msgs/Pose.h>
#include <cubic_polynomial_planner/PosePlanner.hpp>


int main(int argc, char **argv){
    ros::init(argc, argv, "pose_planner");

    ros::NodeHandle nh ;

    ros::Rate loopRate(500);

    PosePlanner planner(nh);

    while ( ros::ok() ) {
        // process callbacks and will not return until the node has been shutdown
        ros::spinOnce() ;
        
        planner.update();

        // sleep for any time remaining to the publish rate
        loopRate.sleep();
	}


    return 0;
}