#include <ros/ros.h>
#include <cubic_polynomial_planner/ActionServer.hpp>

int main(int argc, char** argv){
    ros::init(argc,argv, "action_client");
    ros::NodeHandle nh;

    ActionServer action_server(nh);
    int pub_rate;
    nh.getParam("/publish_rate",pub_rate);
    ros::Rate loop_rate(500);
    
    while ( ros::ok() ) {
        // if (! action_server.ready()){
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
        // else{
        //     ros::spinOnce();
        //     action_server.update() ;
        //     loop_rate.sleep();
        // }
        ros::spinOnce();
        action_server.update() ;
        loop_rate.sleep();
	}
    return 0;
}