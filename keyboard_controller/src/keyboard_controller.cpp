#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/String.h>

// Command message to publish
geometry_msgs::Twist twist_cmd; 
bool pressed;

void callback(const std_msgs::String& msg){
    // ROS_INFO("Reading Data ");
    twist_cmd.linear.x = 0.0;
    twist_cmd.angular.z = 0.0;
    pressed = true;

    if (msg.data.compare("i")== 0){ // Forward
        // ROS_INFO("Forward");
        twist_cmd.linear.x = 0.5;
        twist_cmd.angular.z = 0.0;
    }
    
    if (msg.data.compare("u")== 0) {// Left
        // ROS_INFO("Left");
        twist_cmd.linear.x = 0.5;
        twist_cmd.angular.z = 0.5;
    }

    if (msg.data.compare("o")== 0) { // Right
        // ROS_INFO("Right");
        twist_cmd.linear.x = 0.5;
        twist_cmd.angular.z = -0.5;
    }
}

int main(int argc, char** argv){

    pressed = false;

    // Initialize Node
    ros::init (argc, argv, "Keyboard_controller");

    // The interface for communications with ROS (topics, services, parameters)
    ros::NodeHandle nh;
    
    // specify the frequency to 1000HZ
    ros::Rate loopRate(1000);

    // Create Subscriber
    ros::Subscriber subscriber = nh.subscribe("teleop/cmd", 5, callback);

    // Create Publisher
    ros::Publisher publisher = nh.advertise<geometry_msgs::Twist>("/husky_velocity_controller/cmd_vel", 5);
    
    while ( ros::ok() ) {

        // process callbacks and will not return until the node has been shutdown
        ros::spinOnce() ;
        
        if (pressed){
            publisher.publish(twist_cmd);
            pressed = false;
        }
        
        // // process callbacks and will not return until the node has been shutdown
        // ros::spinOnce() ;

        // sleep for any time remaining to the publish rate
        loopRate.sleep();
	}

    return 0;
}
