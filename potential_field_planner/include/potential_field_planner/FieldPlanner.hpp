#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>

class FieldPlanner {
    public:
        // Constructor
        FieldPlanner(ros::NodeHandle& nh);
        // Computes new pose
        // Assumes valid inputs, callback handles bad inputs
        bool service_callback(std_srvs::Trigger::Request  &req,
				 	 std_srvs::Trigger::Response &res);

        void sub_callback(const geometry_msgs::Pose cur_pos);

        void compute_field(); 

        void update();

    protected:

        bool read_parameters();


    private:
        // ROS Stuff
        ros::NodeHandle& node_handle_ ;
        ros::ServiceServer service_;
        ros::Subscriber sub_;

        ros::Publisher pose_pub_;
        ros::Publisher twist_pub_;
        ros::Publisher done_pub_;
        ros::Publisher serve_pub_;

        //Parameter Values
        Eigen::MatrixXd K_ ;

        double k_att_ ;
        double x_target_;
        double y_target_;

        double max_lin_vel_;

        // Service Stuff
        geometry_msgs::Pose cur_pose_;
        
        // geometry_msgs::Twist cur_twist_;

        // Publish Stuff
        bool planning_done_;
        bool can_serve_;
        std_msgs::Bool Reached_;
        geometry_msgs::Pose des_pose_;
        geometry_msgs::Twist des_twist_;
        std_msgs::Bool Serve_;

        // Field Stuff
        Eigen::Vector2d v_pose_;
        Eigen::Vector2d v_twist_;
        double x_dist;
        double y_dist;

        // Debug
        double start_time;
        double end_time;
        bool startup;
};
