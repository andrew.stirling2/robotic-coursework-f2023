#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>
#include <cmath>
#include <potential_field_planner/FieldPlanner.hpp>

FieldPlanner::FieldPlanner(ros::NodeHandle& nh) : node_handle_(nh), planning_done_(false), can_serve_(false), startup(true){
    // Service
    service_ = node_handle_.advertiseService("planner/start", &FieldPlanner::service_callback, this);
    // Publishers Initialization
    pose_pub_ = node_handle_.advertise<geometry_msgs::Pose>("planner/pose",1);
    twist_pub_ = node_handle_.advertise<geometry_msgs::Twist>("planner/twist",1);
    done_pub_ = node_handle_.advertise<std_msgs::Bool>("planner/done",1);
    serve_pub_ = node_handle_.advertise<std_msgs::Bool>("planner/serve",1);
    // Subscriber Init
    sub_ = node_handle_.subscribe("/husky_velocity_controller/feedback/pose", 1, &FieldPlanner::sub_callback,this);

    K_ = Eigen::MatrixXd::Identity(2,2);

    if (read_parameters()) {
		ROS_INFO("Successfully read parameters");
        K_ = K_ * k_att_;
        // ROS_INFO_STREAM("K_att Matrix = " << K_);
	}
    
    ROS_INFO("Connected with potential_field_planner");

}
bool FieldPlanner::service_callback(std_srvs::Trigger::Request  &req,
				 	 std_srvs::Trigger::Response &res){
    
    
    x_dist = x_target_ - cur_pose_.position.x;
    y_dist = y_target_ - cur_pose_.position.y;

    if (sqrt(pow(x_dist,2) + pow(y_dist,2)) < 0.1 ){
        planning_done_ = true;
        can_serve_ = false;
        ROS_INFO("Position (%f, %f) is already within 0.1m of target position", cur_pose_.position.x,cur_pose_.position.y);
    }
    else{
        can_serve_ = true;
    }
    
    res.success = true;
    res.message = "Succesfully called callback, with target position: " + std::to_string(x_target_) + " , " + std::to_string(y_target_);
    start_time = ros::Time::now().toSec();
    return true;
}
void FieldPlanner::sub_callback(const geometry_msgs::Pose pose){
    cur_pose_ = pose;
    // ROS_INFO("Subscriber Succesfully Called");
}
void FieldPlanner::compute_field(){
    float delta_t = 1 / 500 ;
    // Twist Calculation
    x_dist = x_target_ - cur_pose_.position.x;
    y_dist = y_target_ - cur_pose_.position.y;
    if (sqrt(pow(x_dist,2) + pow(y_dist,2)) < 0.1){
        planning_done_ = true;
        can_serve_ = false;
        ROS_INFO("Arrived at (%f,%f)", cur_pose_.position.x,cur_pose_.position.y);
        return;
    }
    v_pose_(0) = x_dist;
    v_pose_(1) = y_dist;
    v_twist_ = K_ * v_pose_ ;
    // ROS_INFO_STREAM("Calculated v_twist: " << v_twist_);
    // Normalize
    // if (v_twist_.norm() >= max_lin_vel_){
    //     v_twist_ = (v_twist_ / v_twist_.norm()) * max_lin_vel_;
    // }
    v_twist_ = (v_twist_ / v_twist_.norm()) * max_lin_vel_;
    // ROS_INFO_STREAM("Normalized v_twist: " << v_twist_);
    // Assign des_twist 
    des_twist_.linear.x = v_twist_(0);
    des_twist_.linear.y = v_twist_(1);

    // Pose Calculation
    des_pose_.position.x = cur_pose_.position.x + (v_twist_(0) * delta_t);
    des_pose_.position.y = cur_pose_.position.y + (v_twist_(1) * delta_t);
    // ROS_INFO_STREAM("Desired Pose x = "<< des_pose_.position.x << " y = "<< des_pose_.position.y);

    // if (sqrt(pow(des_pose_.position.x - x_target_,2)+pow(des_pose_.position.y-y_target_,2)) < 0.1){
    //     planning_done_ = true;
    //     can_serve_ = false;
    // }
    
    return;
}
void FieldPlanner::update(){
    
    if(startup){
        ROS_INFO("Entered startup");
        startup = false;
        x_dist = x_target_ - cur_pose_.position.x;
        y_dist = y_target_ - cur_pose_.position.y;

        if (sqrt(pow(x_dist,2) + pow(y_dist,2)) < 0.1 ){
            planning_done_ = true;
            can_serve_ = false;
            ROS_INFO("Position (%f, %f) is already within 0.1m of target position", cur_pose_.position.x,cur_pose_.position.y);
        }
    }

    // if service called can start publishing
    if (can_serve_){
        compute_field();
        pose_pub_.publish(des_pose_);
        twist_pub_.publish(des_twist_);
        
    }
    Reached_.data = planning_done_;
    done_pub_.publish(Reached_);
    Serve_.data = can_serve_;
    serve_pub_.publish(Serve_);

    
    // ROS_INFO_STREAM("Succesfully published " << planning_done_);
}
bool FieldPlanner::read_parameters() {
    if ( !node_handle_.getParam("planner/k_att", k_att_ )) {
		ROS_INFO("Cannot read k_att" );
		return false ;
	}
    
    if ( !node_handle_.getParam("/husky_velocity_controller/linear/x/max_velocity", max_lin_vel_)) {
		ROS_INFO("Cannot y target" );
		return false ;
	}
    if ( !node_handle_.getParam("target/x", x_target_)) {
		ROS_INFO("Cannot x target" );
		return false ;
	}
    if ( !node_handle_.getParam("target/y", y_target_)) {
		ROS_INFO("Cannot y target" );
		return false ;
	}
	ROS_INFO("k_att_: %f", k_att_ );
    ROS_INFO("Max Linear Velocity = %f", max_lin_vel_);
    return true;
}