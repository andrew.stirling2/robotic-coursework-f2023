#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>
#include <potential_field_planner/FieldPlanner.hpp>


int main(int argc, char **argv){
    // initialize ROS
	ros::init(argc, argv, "planner");

    ros::NodeHandle nh;
    
    ros::Rate loopRate(500);
    
    FieldPlanner planner(nh);

    while (ros::ok()){
        // Call callbacks
        ros::spinOnce();
        // Plan and publish
        planner.update();

        loopRate.sleep();
    }
    
    
    return 0;
}