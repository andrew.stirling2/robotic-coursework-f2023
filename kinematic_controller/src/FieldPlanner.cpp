#include <ros/ros.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <kinematic_controller/FieldPlanner.hpp>
#include <cmath>

FieldPlanner::FieldPlanner(ros::NodeHandle& nh) : node_handle_(nh), k_att_(150){
    // Subscriber Init
    sub_ = node_handle_.subscribe("/gen3/joint_states", 1, &FieldPlanner::sub_callback,this);

    // Publishers Initialization
    pose_pub_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/gen3/joint_group_position_controller/command",1);
    
    target_.resize(7,0.0);
    des_vel_.resize(7,0.0);
    des_pos_.resize(7,0.0);
    cur_pos_.resize(7,100.0);
    eig_dist = Eigen::VectorXd(7);
    eig_vel = Eigen::VectorXd(7);
    K_ = Eigen::MatrixXd::Identity(7,7);
    K_ = K_ * k_att_;

    vel_limits_[0] = 1.3963;
    vel_limits_[1] = 1.3963;
    vel_limits_[2] = 1.3963;
    vel_limits_[3] = 1.3963;
    vel_limits_[4] = 1.2218;
    vel_limits_[5] = 1.2218;
    vel_limits_[6] = 1.2218;
    if (read_parameters()){
        ROS_INFO("Succesfully read parameters");
    }
    ROS_INFO("Connected with planner");
    ROS_INFO("Using k_att = %i", k_att_);
    
}
void FieldPlanner::sub_callback(const sensor_msgs::JointState cur_state){
    cur_pos_ = cur_state.position;
}
void FieldPlanner::compute_field(){
    for(int i = 0; i<7;i++){
        eig_dist(i) = target_[i] - cur_pos_[i]; 
    }
    
    eig_vel = K_ * eig_dist;
    double eig_element;
    for(int i = 0; i<7;i++){
        eig_element = eig_vel(i);
        
        if(abs(eig_element) > vel_limits_[i]){
            des_vel_[i] = eig_element/abs(eig_element)*vel_limits_[i];
            // ROS_INFO("Exceeded velocity limits");
        }
        else{
            des_vel_[i] = eig_element;
        }

        des_pos_[i] = cur_pos_[i] + 2.0*(des_vel_[i]/pub_rate_);

    }
    return;
}
void FieldPlanner::update(){
    // ROS_INFO("Starting update");
    compute_field();
    // cmd_pose_.layout.dim[0].label = "Desired Joint Positions";
    // cmd_pose_.layout.dim[0].size = 7;
    ROS_INFO("Position Error is = %f", eig_dist.norm());
    cmd_pose_.data = des_pos_;
    pose_pub_.publish(cmd_pose_);
    // ROS_INFO_STREAM("Desired Position: " << cmd_pose_);
    // ROS_INFO("Done Update");
}
bool FieldPlanner::read_parameters() {
    
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/joint/positions", target_)) {
		ROS_INFO("Can't read target joint positions");
		return false;
	}
    ROS_INFO("Target position: [%f,%f,%f,%f,%f,%f,%f]", target_[0], target_[1], target_[2],
     target_[3], target_[4], target_[5], target_[6]);
    ROS_INFO_STREAM("Publishing at: " << pub_rate_ << " Hz");
    return true;
}

bool FieldPlanner::ready(){
    if(cur_pos_[0]>90.0){
        return false;
    }
    return true;
}
