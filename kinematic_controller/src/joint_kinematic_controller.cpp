#include <ros/ros.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>
#include <kinematic_controller/FieldPlanner.hpp>

int main(int argc, char **argv){
    // initialize ROS
	ros::init(argc, argv, "joint_controller");

    ros::NodeHandle nh;

    double pub_rate;
    nh.getParam("/publish_rate",pub_rate);
    ros::Rate loopRate(pub_rate);
    
    FieldPlanner planner(nh);
    // int count = 1;
    while (ros::ok()){
        if(! planner.ready()){
            ros::spinOnce();
            loopRate.sleep();
            // ROS_INFO_STREAM("WAITNG COUNT = " << count);
            // count++;
        }
        else{
            // Call callbacks
            ros::spinOnce();
            
            // Plan and publish
            planner.update();

            loopRate.sleep();
        }
        
    }
    
    
    return 0;
}