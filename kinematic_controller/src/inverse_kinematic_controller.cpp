#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <highlevel_msgs/MoveTo.h>
#include <kinematic_controller/CubicPlanner.hpp>

int main(int argc, char **argv){
    // initialize ROS
	ros::init(argc, argv, "task_controller");

    ros::NodeHandle nh;

    double pub_rate;
    nh.getParam("/publish_rate",pub_rate);
    ros::Rate loopRate(pub_rate);
    
    CubicPlanner planner(nh);
    int count = 0;
    while (ros::ok()){
        if(!planner.ready()){
            ROS_INFO_STREAM("WAITING: " << count);
            count++;
            ros::spinOnce();
            loopRate.sleep();
        }

        else{
            // Call callbacks
            ros::spinOnce();
            
            // Plan and publish
            planner.update();

            loopRate.sleep();
        }
        
    }
    
    
    return 0;
}