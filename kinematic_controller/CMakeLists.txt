cmake_minimum_required(VERSION 3.10)
project(kinematic_controller)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
# add_definitions(-Wall -Werror)

# Eigen is not a catkin library , "find" i t separately
find_package(Eigen3 REQUIRED)

find_package(pinocchio REQUIRED) #find pinocchio
add_definitions("-DBOOST_MPL_LIMIT_LIST_SIZE=30")  # something for pinocchio

## Find catkin macros and libraries
find_package(catkin REQUIRED
  COMPONENTS
    roscpp
    std_srvs
    std_msgs
    geometry_msgs
    tf2_geometry_msgs     
    sensor_msgs  
    highlevel_msgs
)


catkin_package(
  INCLUDE_DIRS    
  	${catkin_INCLUDE_DIRS}
    ${EIGEN_INCLUDE_DIRS}
    ${PINOCCHIO_INCLUDE_DIRS}
  LIBRARIES 
    pinocchio
  CATKIN_DEPENDS
    roscpp
    std_srvs
    std_msgs
    geometry_msgs
    tf2_geometry_msgs
    sensor_msgs
    highlevel_msgs
  DEPENDS
 
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS} 
  ${EIGEN_INCLUDE_DIRS} # include non−catkin directory separately
  ${PINOCCHIO_INCLUDE_DIRS} # include pinocchio
)
# For joint_kinematic_controller
add_executable(	joint_kinematic_controller 
src/joint_kinematic_controller.cpp
src/FieldPlanner.cpp
)
target_link_libraries(joint_kinematic_controller 
${catkin_LIBRARIES}
)

# For inverse kinematic_controller
add_executable(inverse_kinematic_controller 
src/inverse_kinematic_controller.cpp
src/CubicPlanner.cpp
)
target_link_libraries(inverse_kinematic_controller 
${catkin_LIBRARIES}
${pinocchio_LIBRARIES}
)
#add_dependencies(		${PROJECT_NAME}_server hello_service_gencpp)

# add_executable(			${PROJECT_NAME}_client src/client.cpp)
# target_link_libraries(	${PROJECT_NAME}_client ${catkin_LIBRARIES})
#add_dependencies(		${PROJECT_NAME}_client hello_service_gencpp)



#############
## Install ##
#############

# Mark executables and/or libraries for installation
# install(
#   TARGETS ${PROJECT_NAME}_server ${PROJECT_NAME}_client
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

# Mark cpp header files for installation
install(
  DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.hpp"
)