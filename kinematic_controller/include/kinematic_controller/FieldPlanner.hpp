#include <ros/ros.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
class FieldPlanner {
    public:
        // Constructor
        FieldPlanner(ros::NodeHandle& nh);

        void sub_callback(const sensor_msgs::JointState cur_state);

        void compute_field(); 

        void update();

        bool ready();

    protected:

        bool read_parameters();

    private:
        // ROS stuff
        ros::NodeHandle& node_handle_;
        ros::Subscriber sub_;
        ros::Publisher pose_pub_;

        // Params
        std::vector<double> target_;
        double vel_limits_[7];
        double pub_rate_;

        // Publish
        std::vector<double> des_vel_;
        std::vector<double> des_pos_;
        std_msgs::Float64MultiArray cmd_pose_;

        // Vars
        std::vector<double> cur_pos_;
        int k_att_;
        Eigen::MatrixXd K_;

        Eigen::VectorXd eig_dist;
        Eigen::VectorXd eig_vel;


};