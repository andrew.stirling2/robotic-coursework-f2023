
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>

#include <Eigen/Dense> 
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <cmath>
#include <highlevel_msgs/MoveTo.h>
#include <dynamic_controller/JointControl.hpp>

JointControl::JointControl(ros::NodeHandle& nh) : node_handle_(nh){
    // Subscriber Init
    sub_ = node_handle_.subscribe("/gen3/joint_states", 1, &JointControl::sub_callback,this);

    // Publishers Initialization
    torque_pub_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/gen3/joint_group_effort_controller/command",1);
    
    target_.resize(7,0.0);
    cur_pos_.resize(7,100.0);
    cur_vel_.resize(7,100.0);
    cur_torque_.resize(7,0.0);
    des_torque_.resize(7,0.0);

    eig_dist = Eigen::VectorXd(7);
    eig_des_pos = Eigen::VectorXd(7);
    eig_des_vel = Eigen::VectorXd(7);
    eig_des_acc = Eigen::VectorXd(7);

    eig_cur_pos = Eigen::VectorXd(7);
    eig_cur_vel = Eigen::VectorXd(7);
    eig_cur_acc = Eigen::VectorXd(7);

    eig_torque = Eigen::VectorXd(7);
    eig_cur_torque = Eigen::VectorXd(7);

    K_ = Eigen::MatrixXd::Identity(7,7);
    

    torque_limits_[0] = 39;
    torque_limits_[1] = 39;
    torque_limits_[2] = 39;
    torque_limits_[3] = 39;
    torque_limits_[4] = 9;
    torque_limits_[5] = 9;
    torque_limits_[6] = 9;

    vel_limits_[0] = 1.3963;
    vel_limits_[1] = 1.3963;
    vel_limits_[2] = 1.3963;
    vel_limits_[3] = 1.3963;
    vel_limits_[4] = 1.2218;
    vel_limits_[5] = 1.2218;
    vel_limits_[6] = 1.2218;
    if (read_parameters()){
        ROS_INFO("Succesfully read parameters");
        pinocchio::urdf::buildModel(urdf_file_, model, false);
        data = pinocchio::Data(model);
    }
    K_ = K_ * k_att_;
    ROS_INFO("Connected with planner");
    ROS_INFO_STREAM("Using k_att = "<< k_att_);
    ROS_INFO_STREAM("Using k_p = "<< kp_);
    ROS_INFO_STREAM("Using k_d = "<< kd_);
    
}
void JointControl::sub_callback(const sensor_msgs::JointState cur_state){
    cur_pos_ = cur_state.position;
    cur_vel_ = cur_state.velocity;
    cur_torque_ = cur_state.effort;
}
void JointControl::compute_field(){
    for(int i = 0; i<7;i++){
        eig_dist(i) = target_[i] - cur_pos_[i]; 
        eig_cur_pos(i) = cur_pos_[i];
        eig_cur_vel(i) = cur_vel_[i];
        eig_cur_torque(i) = cur_torque_[i];
    }
    
    eig_des_vel = K_ * eig_dist;
    // for(int i =0;i<7;i++){
    //     if (abs(eig_des_vel(i)) > vel_limits_[i]){
    //         eig_des_vel(i) = eig_des_vel(i)/abs(eig_des_vel(i)) * vel_limits_[i];
    //     }
    // }
    eig_des_pos = eig_cur_pos + 2.0*eig_des_vel/pub_rate_;
    eig_des_acc = k_acc_*(eig_des_vel - eig_cur_vel) * pub_rate_;
    return;
}
void JointControl::compute_torque(){
    // ROS_INFO_STREAM("Reference Acceleration = " << eig_des_acc.transpose());
    // ROS_INFO_STREAM("Desired Position: "<< eig_des_pos.transpose());
    // ROS_INFO_STREAM("Current Position: "<< eig_cur_pos.transpose());
    // ROS_INFO_STREAM("Position Error = " << (eig_des_pos-eig_cur_pos).transpose());
    // ROS_INFO_STREAM("Velocity Error = " << (eig_des_vel-eig_cur_vel).transpose());

    eig_des_acc =  eig_des_acc + kd_*(eig_des_vel-eig_cur_vel) + kp_*(eig_des_pos-eig_cur_pos);

    // eig_des_acc = kd_*(eig_des_vel-eig_cur_vel) + kp_*(eig_des_pos-eig_cur_pos);

    pinocchio::computeAllTerms(model,data,eig_cur_pos,eig_cur_vel);
    
    // Check this
    eig_torque =  (data.M * eig_des_acc) + data.nle;

    for (int i =0; i<7;i++){
        des_torque_[i] = eig_torque(i);
    }

}

void JointControl::update(){
    // ROS_INFO("Starting update");
    compute_field();
    compute_torque();
    // ROS_INFO("Position Error is = %f", eig_dist.norm());
    cmd_torque_msg_.data = des_torque_;
    torque_pub_.publish(cmd_torque_msg_);
    // ROS_INFO_STREAM("Torque Command " << eig_torque.transpose());
    // ROS_INFO("Done Update");
}
bool JointControl::read_parameters() {
    if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_)) {
		ROS_INFO("Can't read urdf file name");
		return false;
	}
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/joint/positions", target_)) {
		ROS_INFO("Can't read target joint positions");
		return false;
	}
    if ( !node_handle_.getParam("/joint_controller/gains/k_att", k_att_)) {
		ROS_INFO("Can't read k_att");
		return false;
	}
    if ( !node_handle_.getParam("/joint_controller/gains/kp", kp_)) {
		ROS_INFO("Can't read kp");
		return false;
	}
    if ( !node_handle_.getParam("/joint_controller/gains/kd", kd_)) {
		ROS_INFO("Can't read kd");
		return false;
	}
    if ( !node_handle_.getParam("/joint_controller/gains/k_acc", k_acc_)) {
		ROS_INFO("Can't read k_acc_");
		return false;
	}
    ROS_INFO("Target position: [%f,%f,%f,%f,%f,%f,%f]", target_[0], target_[1], target_[2],
     target_[3], target_[4], target_[5], target_[6]);
    ROS_INFO_STREAM("Publishing at: " << pub_rate_ << " Hz");
    return true;
}

bool JointControl::ready(){
    if(cur_pos_[0]>90.0){
        return false;
    }
    if(cur_vel_[0]>90.0){
        return false;
    }
    return true;
}
