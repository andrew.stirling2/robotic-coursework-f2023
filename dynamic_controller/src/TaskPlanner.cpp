// #pragma GCC diagnostic push
// #pragma GCC diagnostic ignored "-Wignored-attributes"
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/compute-all-terms.hpp>
// #pragma GCC diagnostic pop
#include <Eigen/Dense>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <cmath>
#include <highlevel_msgs/MoveTo.h>
#include <dynamic_controller/TaskPlanner.hpp>


TaskPlanner::TaskPlanner(ros::NodeHandle& nh) : node_handle_(nh), JOINT_ID(7), T_(4.5), has_start_pos_(false), arrived_(false), service_called(false){
    // Subscriber Init
    sub_ = node_handle_.subscribe("/gen3/joint_states", 1, &TaskPlanner::sub_callback,this);

    // Service Init
    service_ = node_handle_.advertiseService("pose_planner/move_to", &TaskPlanner::service_callback,this);

    // Publisher Initialization
    torque_pub_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/gen3/joint_group_effort_controller/command",1);

    target_.resize(3,0.0);
    des_torque_.resize(7,0.0);
    cur_joint_pos_.resize(7,100.0);
    cur_joint_vel_.resize(7,0.0);

    vel_limits_[0] = 1.3963;
    vel_limits_[1] = 1.3963;
    vel_limits_[2] = 1.3963;
    vel_limits_[3] = 1.3963;
    vel_limits_[4] = 1.2218;
    vel_limits_[5] = 1.2218;
    vel_limits_[6] = 1.2218;

    eig_start_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_target_joint = Eigen::VectorXd::Zero(7);
    eig_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_joint_vel_ = Eigen::VectorXd::Zero(7);
    eig_des_joint_ang_ = Eigen::VectorXd::Zero(7);
    eig_des_joint_vel_ = Eigen::VectorXd::Zero(7);
    eig_des_pos = Eigen::Vector3d::Zero();
    eig_des_vel = Eigen::Vector3d::Zero();
    eig_target = Eigen::Vector3d::Zero();
    eig_start_pos = Eigen::Vector3d::Zero();
    eig_null_torque = Eigen::VectorXd::Zero(7);
    eig_arrived_joint_pos = Eigen::VectorXd::Zero(7);
    eig_cur_pos = Eigen::Vector3d::Zero();
    
    jacobian_local_world = Eigen::MatrixXd::Zero(6, JOINT_ID);
    jacobian_local_translation = Eigen::MatrixXd::Zero(3,JOINT_ID);
    jacobian_local_pseudo = Eigen::MatrixXd::Zero(JOINT_ID,3);
    jacobian_dot = Eigen::MatrixXd::Zero(6,JOINT_ID);
    jacobian_dot_translation = Eigen::MatrixXd::Zero(3,JOINT_ID);

    t_initial_ = ros::Time::now().toSec();

    ROS_INFO("Reading Parameters...");
    if(readParamaters()){
        ROS_INFO("Succesfully Read Parameters!");
        pinocchio::urdf::buildModel(urdf_file_, model, false);
        data = pinocchio::Data(model);
    }
    
    for(int i =0;i<7;i++){
        eig_target_joint(i) = target_joints[i];
    }
    eig_target(0) = target_[0];
    eig_target(1) = target_[1];
    eig_target(2) = target_[2];
    

    ROS_INFO("Succesfully connected with Cubic Planner");
    ROS_INFO_STREAM("Using k_att = "<< k_att_);
    ROS_INFO_STREAM("Using k_p_field = "<< Kp);
    ROS_INFO_STREAM("Using k_d_field = "<< Kd);
    ROS_INFO_STREAM("Using k_p_null = "<< Kp_null);
    ROS_INFO_STREAM("Using k_acc = "<< k_acc_);
}

bool TaskPlanner::service_callback(highlevel_msgs::MoveTo::Request  &req, highlevel_msgs::MoveTo::Response &res){
    if (req.z < 0) {
        res.can_execute = false;
        ROS_INFO("move_to z position must be greater than 0");
        return false;
    }
    if (req.T <= 0){
        res.can_execute = false;
        ROS_INFO_STREAM("move_to period T must be greater than 0");
        return false;
    }
    target_[0] = req.x;
    target_[1] = req.y;
    target_[2] = req.z;
    eig_target(0) = target_[0];
    eig_target(1) = target_[1];
    eig_target(2) = target_[2];
    T_ = req.T;
    res.can_execute = true;
    has_start_pos_ = false;
    arrived_ = false;
    service_called = true;
    // ROS_INFO_STREAM("Current Position: " << eig_cur_pos.transpose());
    

    return true;
}

void TaskPlanner::sub_callback(const sensor_msgs::JointState cur_state){

    cur_joint_pos_ = cur_state.position;
    cur_joint_vel_ = cur_state.velocity;
    // eig_des_joint_ang_ = Eigen::Vector
    for (int i = 0; i<cur_joint_pos_.size();i++){
        eig_joint_ang_(i) = cur_joint_pos_[i];
        eig_joint_vel_(i) = cur_joint_vel_[i];
    }

}

void TaskPlanner::compute_traj(){
    if (! has_start_pos_){
        // Assign start position of hand
        compute_start_pos();
        t_initial_ = ros::Time::now().toSec();
        has_start_pos_ = true;
        
        ROS_INFO_STREAM("Desired Position: " << eig_target.transpose());
    }

    offset_time_ = ros::Time::now().toSec();
    cur_time_ =  offset_time_ - t_initial_;
    if (cur_time_ > T_){
        // eig_des_pos = eig_target;
        // eig_des_vel = 200*(eig_target - eig_cur_pos);
        ROS_INFO_STREAM("Time greater than T");
        compute_field();
        return;
    }

    time_coeff_ = (3*pow(cur_time_,2)/pow(T_,2)) - (2*pow(cur_time_,3)/pow(T_,3));

    eig_des_pos = eig_start_pos + (time_coeff_*(eig_target - eig_start_pos));
    // ROS_INFO_STREAM("Start Pos: " << eig_start_pos.transpose());
    // ROS_INFO_STREAM("Desired Pos: " << eig_des_pos.transpose());
    fwd_kin();
    // eig_des_vel = ((6*cur_time_/pow(T_,2)) - (6*pow(cur_time_,2)/pow(T_,3))) * (eig_target - eig_start_pos);
    eig_des_vel = k_att_*(eig_des_pos - eig_cur_pos);
    eig_des_acc = ((6/pow(T_,2)) - (12*cur_time_/pow(T_,3)))* (eig_target - eig_start_pos);
    eig_des_acc *= k_acc_;
    // Get feedback position
    eig_fbk_vel = jacobian_local_translation * eig_joint_vel_;


    return;
}

void TaskPlanner::compute_inv_dyn(){
    // ROS_INFO_STREAM("Using Kp = "<< Kp);
    // ROS_INFO_STREAM("Using Kd = "<< Kd);
    // ROS_INFO_STREAM("position error: "<<(eig_des_pos-eig_cur_pos).transpose());
    // ROS_INFO_STREAM("velocity error: "<<(eig_des_vel-eig_fbk_vel).transpose());
    // ROS_INFO_STREAM("x_ddot reference: "<<eig_des_acc.transpose());

    eig_des_acc = eig_des_acc + (Kd*(eig_des_vel-eig_fbk_vel)) + (Kp*(eig_des_pos - eig_cur_pos));
    // Lambda Matrix
    end_effec_wrench = jacobian_local_pseudo.transpose()* data.M * jacobian_local_pseudo * eig_des_acc;
    // Eta Matrix
    end_effec_wrench = end_effec_wrench + (jacobian_local_pseudo.transpose()*data.nle) -  jacobian_local_pseudo.transpose()* data.M * jacobian_local_pseudo * jacobian_dot_translation * eig_joint_vel_;
    eig_cmd_torque = jacobian_local_translation.transpose()*end_effec_wrench;

    // ROS_INFO_STREAM("cmd torque pre null: " << eig_cmd_torque.transpose());
    
    // Add Redundancy
    eig_null_torque = (data.M * (Kp_null * (eig_target_joint - eig_joint_ang_) + Kd*(-1*eig_joint_vel_))) + data.nle;
    eig_null_torque = jacobian_nullspace*eig_null_torque;

    eig_cmd_torque = eig_cmd_torque + eig_null_torque;
    // ROS_INFO_STREAM("cmd torque post null: " << eig_cmd_torque.transpose());
    // ROS_INFO_STREAM("Current hand position: " << eig_cur_pos.transpose());
    for (int i = 0; i<7;i++){
        des_torque_[i] = eig_cmd_torque(i);
    }
    if ((eig_cur_pos - eig_target).norm() < 0.01){
        if(!arrived_){
            ROS_INFO_STREAM("Arrived with error = " << (eig_cur_pos - eig_target).norm());
        }
        arrived_ = true;
    }
}
void TaskPlanner::compute_start_pos(){
    eig_start_joint_ang_ = eig_joint_ang_;
    pinocchio::forwardKinematics(model,data,eig_joint_ang_,eig_joint_vel_);
    pinocchio::SE3 pose_now = data.oMi[JOINT_ID];
    eig_start_pos = pose_now.translation();
}

void TaskPlanner::compute_field(){
    // Get eig_cur_pos in task space
    fwd_kin(); 
    
    // Get feedback position
    eig_fbk_vel = jacobian_local_translation * eig_joint_vel_;

    // All in task space, all reference velocities
    eig_des_vel = k_att_ * (eig_target - eig_cur_pos);

    // Normalize linear velocity
    if (eig_des_vel.norm() > max_lin_vel){
        eig_des_vel = eig_des_vel/eig_des_vel.norm() * max_lin_vel;
    }

    eig_des_pos = eig_cur_pos + 2.0*eig_des_vel/pub_rate_;
    eig_des_acc = k_acc_*(eig_des_vel - eig_fbk_vel) * pub_rate_;


}

void TaskPlanner::compute_all_jacob(){
    

    pinocchio::computeAllTerms(model, data, eig_joint_ang_, eig_joint_vel_);
	pinocchio::getJointJacobian(model, data, JOINT_ID, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_local_world);
    
    jacobian_local_translation = jacobian_local_world.topRows(3);
    jacobian_local_pseudo = jacobian_local_translation.transpose() * (jacobian_local_translation * jacobian_local_translation.transpose()).inverse();

    pinocchio::computeJointJacobiansTimeVariation(model, data, eig_joint_ang_, eig_joint_vel_);
	pinocchio::getJointJacobianTimeVariation(model, data, JOINT_ID, pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, jacobian_dot) ;
    jacobian_dot_translation = jacobian_dot.topRows(3);

    jacobian_nullspace = Eigen::MatrixXd::Identity(7,7) - (jacobian_local_translation.transpose()*(jacobian_local_translation*data.M.inverse()*jacobian_local_translation.transpose()).inverse()*jacobian_local_translation*data.M.inverse());
}


void TaskPlanner::update(){

    compute_all_jacob(); // Compute all relevant jacobians required for each iteration

    compute_field(); // Compute potential field

    compute_inv_dyn(); // Compute desired joint_position 

    cmd_torque_msg_.data = des_torque_;
    torque_pub_.publish(cmd_torque_msg_);
    
}

void TaskPlanner::fwd_kin(){
    pinocchio::forwardKinematics(model,data,eig_joint_ang_,eig_joint_vel_);
    eig_cur_pos = data.oMi[JOINT_ID].translation();
}

bool TaskPlanner::ready(){
    if(ros::Time::now().toSec() == 0){
        return false;
    }
    if(cur_joint_pos_[0] > 90.0){
        return false;
    }
    return true;
}
bool TaskPlanner::readParamaters(){
    if ( !node_handle_.getParam("/publish_rate", pub_rate_)) {
		ROS_INFO("Can't read publish rate");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/hand/position", target_)) {
		ROS_INFO("Can't read target hand positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/target/joint/positions", target_joints)) {
		ROS_INFO("Can't read target joint positions");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/urdf_file_name", urdf_file_)) {
		ROS_INFO("Can't read urdf file name");
		return false;
	}
    if ( !node_handle_.getParam("/gen3/linear/max_velocity", max_lin_vel)) {
		ROS_INFO("Can't read max velocity");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_att", k_att_)) {
		ROS_INFO("Can't read k_att");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kp_field", Kp)) {
		ROS_INFO("Can't read kp");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kd_field", Kd)) {
		ROS_INFO("Can't read kd");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/k_acc", k_acc_)) {
		ROS_INFO("Can't read k_acc_");
		return false;
	}
    if ( !node_handle_.getParam("/task_controller/gains/kp_null", Kp_null)) {
		ROS_INFO("Can't read K null");
		return false;
	}
    ROS_INFO("Target Hand Position: [%f,%f,%f]", target_[0], target_[1], target_[2]);
    ROS_INFO_STREAM("Publishing at: " << pub_rate_ << " Hz");
    return true;

}