#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <Eigen/Dense> 
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <highlevel_msgs/MoveTo.h>

class JointControl {
    public:
        // Constructor
        JointControl(ros::NodeHandle& nh);

        void sub_callback(const sensor_msgs::JointState cur_state);

        void compute_field(); 

        void compute_torque();

        void update();

        void fwd_kin();
        
        bool ready();

    protected:

        bool read_parameters();

    private:
        // ROS stuff
        ros::NodeHandle& node_handle_;
        ros::Subscriber sub_;
        ros::Publisher torque_pub_;

        // Params
        std::vector<double> target_;
        std::string urdf_file_;
        double torque_limits_[7];
        double vel_limits_[7];
        double pub_rate_;
        double kd_;
        double kp_;
        double k_acc_;

        // Publish
        Eigen::VectorXd eig_torque;
        std::vector<double> des_torque_;
        std_msgs::Float64MultiArray cmd_torque_msg_;

        // Vars
        std::vector<double> cur_pos_;
        std::vector<double> cur_vel_;
        std::vector<double> cur_torque_;

        double k_att_;
        Eigen::MatrixXd K_;

        Eigen::VectorXd eig_dist;
        Eigen::VectorXd eig_des_pos;
        Eigen::VectorXd eig_des_vel;
        Eigen::VectorXd eig_des_acc;

        Eigen::VectorXd eig_cur_pos;
        Eigen::VectorXd eig_cur_vel;
        Eigen::VectorXd eig_cur_acc;

        Eigen::VectorXd eig_cur_torque;

        // Pinocchio Vars
        pinocchio::Model model;
        pinocchio::Data data;

};