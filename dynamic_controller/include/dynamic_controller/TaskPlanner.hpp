#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <ros/ros.h>
#include <Eigen/Dense>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>
#include <highlevel_msgs/MoveTo.h>

class TaskPlanner{
    public:

        TaskPlanner(ros::NodeHandle& nh);

        bool service_callback(highlevel_msgs::MoveTo::Request  &req,
				 	 highlevel_msgs::MoveTo::Response &res);

        void sub_callback(const sensor_msgs::JointState cur_state);

        void compute_traj();

        void compute_field();

        void compute_inv_dyn();

        void compute_start_pos();

        void compute_all_jacob();

        void fwd_kin();

        void update();

        bool ready();

    protected:

        bool readParamaters();

    private:
        ros::NodeHandle& node_handle_;
        ros::Subscriber sub_;
        ros::ServiceServer service_;
        ros::Publisher torque_pub_;

        // Params
        std::vector<double> target_;
        std::vector<double> target_joints;
        double pub_rate_;
        std::string urdf_file_;
        double max_lin_vel;
        double Kp;
        double Kd;
        double k_acc_;
        double k_att_;
        double Kp_null;

        // To Publish
        std::vector<double> des_torque_;
        std_msgs::Float64MultiArray cmd_torque_msg_;
        Eigen::VectorXd eig_cmd_torque;
        Eigen::VectorXd eig_null_torque;


        // Pinocchio Vars
        pinocchio::Model model;
        pinocchio::Data data;
        // Eigen
        Eigen::Vector3d eig_start_pos;
        Eigen::VectorXd eig_start_joint_ang_;
        Eigen::VectorXd eig_joint_ang_;
        Eigen::VectorXd eig_joint_vel_;
        Eigen::Vector3d eig_des_pos;
        Eigen::Vector3d eig_des_vel;

        
        Eigen::VectorXd eig_des_joint_ang_;
        Eigen::VectorXd eig_des_joint_vel_;
        Eigen::Vector3d eig_target;
        Eigen::VectorXd eig_target_joint;
        Eigen::VectorXd eig_null_joint;
        Eigen::Vector3d eig_cur_pos;

        Eigen::Vector3d eig_des_acc;
        Eigen::Vector3d eig_fbk_vel;
        Eigen::Vector3d end_effec_wrench;

        Eigen::VectorXd eig_arrived_joint_pos;
        const int JOINT_ID;

        // Jacobians 
        Eigen::MatrixXd jacobian_local_world;
        Eigen::MatrixXd jacobian_local_translation;
        Eigen::MatrixXd jacobian_local_pseudo;
        Eigen::MatrixXd jacobian_nullspace;
        Eigen::MatrixXd jacobian_dot;
        Eigen::MatrixXd jacobian_dot_translation;
        // Service Stuff
        
        double T_;
        bool has_start_pos_;


        // Vars
        std::vector<double> cur_joint_pos_;
        std::vector<double> cur_joint_vel_;
        double t_initial_;
        double offset_time_;
        double cur_time_;
        double time_coeff_;
        double vel_limits_[7];
        bool arrived_;
        bool service_called;

};